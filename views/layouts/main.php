<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

$this->registerCssFile("css/gci.css");
$this->registerCssFile("css/pure-min.css");
$this->registerCssFile("Font-Awesome/css/font-awesome.min.css");
$this->registerCssFile("textAngular/dist/textAngular.css");
$this->registerJsFile("js/angular.min.js");
$this->registerJsFile("textAngular/dist/textAngular-rangy.min.js");
$this->registerJsFile("textAngular/dist/textAngular-sanitize.min.js");
$this->registerJsFile("textAngular/dist/textAngular.min.js");
$this->registerJsFile("js/jquery-2.1.4.js");
$this->registerJsFile("js/URI.min.js");
$this->registerJsFile("js/gci.js");
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body ng-app="gciApp">
<?php $this->beginBody() ?>
	<div id="warn">
		<div class="box success">
		</div>
		<div class="box error">
		</div>
	</div>
	<div class="header" ng-controller="gciHeaderController">
		<div class="container">
			<div class="pure-g">
				<div class="pure-u-1-4">
					<h2>{{applicationTitle}}</h2>
				<!-- Gmail Login icon by Icons8 -->
				<img ng-click="getUserRouteDynamic()" class="icon icons8-Gmail-Login" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAIjUlEQVR4Xu2baWxU1xXHz/jN6lk842WoXdYQ6piGLaENTRVqoIlSKFETEdRWMosUQSA0DSTKl9B+aMmXKMVpSqCgSGxSVRGUtCKQKCngEjVNWlK2BEMdwlq7Hi8znsWzz/SeMQ/mjee9uXeu7drDvV8sa86599zfO/93zrtvRgdicBHQcXkLZxAAOZNAABQAOQlwuosMFAA5CXC6iwwUADkJcLqLDCwlgFO2B9I0+7m83j5qLvyoCQTBCYA06aNhIwAKgJwEON1FBgqAnAQ43UUGCoCcBDjdRQYKgJwEON1FBmoAnNycdoIpOKsA4xbKa9CoaRe1nbmyUeejnIvLbEQf5SZv9+/RgW4lV8QFnNOQ3ntlvWPVcK6RPfeIAsSFhxPiSMPD/Yw8wAEpt5CFC8mZKYnIMc4ZiNoaR0q6cnDUAD3rVr5runFtkRQKmVMmE3SufgaCM+cybVI2DsXLYPMpF1wN6ovyz3WaZEvAljlesBpSRc1nO3sSxu1+A8qiUUharZHo+IlH3Tv2/pBmsoIAezeumWG48MVpXYzsOmf4FjwKXctW0KwzyMYTkWDTPyqhPzFoWqb5yvUp2PrtXnCbk0x+snHNwX3gPP7+IN+0yZSI1zfcV9m865zWxJoA21/dMsn+4eGvdImk6i7J1YKONc9DvKqGeQOXA3r4BcnEYiEivF+TzJtiTzCvbejpgtpdvwGiKlXftF5KBR5eclfdC5uvqhlpAuxpWnbB1H6tvlB0SUs5eFY8XZSkP/cZ4Zf/chVaIu/nv7rPC/c6Y8y+KFn3vt+DFO4v6Butm3ixav/Be4oC2DX/WymLQVdQ5vLkvUuWQc/iJwoGlWtwrMMC21odTH4bGvywsDbM5IPGapJVmygcT6drTvxTVYGqcFqf27AncfyjlU6LBC4r/c0+PK0BOtZugqTFyrS5Q9fLYXebncpn9bQALJ1QOHuyJ5PCIajduRUsba1Ua6CRN5QAXzgJ+gUP7W14bduqfI6qAM9tWPsOfPTJj9DJbNBBjdUAej1dMqKkEWJ42nTqYNHwdZKFLSQbtUYjybpnSfaxDEvb+Qw8GsnivIlEGrpCcYjEb77jemjen2Zs2/l40QDRsYywq7bpwWqSqGMvRtJaEIuBV3Xkbag8fJA65lA0Cd3BBKSy3w8WA/DLFzf9NvmXY8/Gkso3jQ5zGTjLJZDK6NoPVkmr9YisvR6rZJOpFPj6k+CPKHtJo6QD6fsLX7/7la0/Z8rAG1tees5x9IPmHnIfCEYHT1pjlcBopMtGVknnQmSFxyrZWCxJJJuE3GSxmcqgitz//Yse2Th+88uvMQN0Hv+wGZ3ypTVK2vLYYnD/7Si1PLDpxuabZsiNNtqyNMqskvV8dxGE/nxEEVLu7cq34GE+gJkbK5Fypz+uuEqXmrfDhMsX4O539pEbNF1LEZw1FzxNa6mqNDbaOGgaZZSse/9OsJ05SXN9yPoW+PLxFXB9yj0wdeP6Wz4o2XEOUjDJX3kMCcBbkxFJe0lpx4EAcZhiEZjz9h6wXbpIFXy8siZTpaPjJ1HZFzIy3biaqbKG3q5CppnPg1Pr4dQTqyBqNGf+lwG6SMvmzNOyDSlAXDBC7hmdgQS0bR0AKI+GT47B148eotoEGrFIWm1SfI7F5ph2/GfRUmidt1BhXv/8eqghHYZZ5Z4+5ABx9RSp839/eQeEI1FFMJXd/4UZB94Eg7eHak8sks6ekFWycVcVnFv+FPRWf00Rl8Vsgu+8tA7K8ManMoYFIK7V9sYfwB/qB58/COn07XYHJT3zyAGo+OIUFURWSbNKtu+bc+Ds4uW3JItB6cgTqtNhA4e1HKY981PNOIcVIK4cSySg2+uDeFx5pHRX62mY9N4B6gLT2fQ0+OfN19wMi2SxUFz9wXL4qmG2Yk6DQYJqlxOM+oEi9X8HmJE0yUCfPwCBkLIa2wM+mHVwN5jb1Y+Nsnfnf2A+dD/ZNKhKo2Sr39oPjk9PUGV1pG4inFm2GgJ2p8LebrWQzLOTJ6vbkh0VAOUo+8mpbndvn0LS+NnMlnepe0Y8Y+xsWnerSqNkx+3foXl2l00Je7uzjcoDZZRsdWUFlJPT9NwxqgBicIlkKiPpaCyuiHVc+xWY/sddVJLGp5fum6fd1aTK0hwEoGTP/3gNdNZNVqxrMhoyktVL+R8/Rx1AOXosLn3BkHIzjD0jlV6JUW5vJ/tV2KyZYqE1Ri1ADDpCshAlnUwpC8w3Tn8MEw+/RctH0+7akifh37MfVNhIZRK4q28XijELUC4w3d4+7p4xFwIWivNLf5K3t6t2VSgKxZgGKAcf7I9Ab59/UM84/dghqPrsY6Zs7Ln/QTi/cOmg3q6ywgG28oFHNNoxqiWcuwm1npH2UCL7ECB77tzejhYe2o0pgPLGMBNZe0at3g4zr9gxJgHiZtV6xnyHEvkOAbR6OxaYYxagXGA8PV7VnhFt1Ho7d5WLulCURBHR2gQeSnj7AgoTPJTAIZ/byR+6KuyZQ4ChGmM6A7MhYIHxdPsG9YyyDUtvxwK3ZADKku71+SEUHsg+eVgtZqh0OoZEsrlwSwqgvDm5Z8T/i+nt7tgMzN44HkrgUDsEYIFU8kVkqGAUM09JSrgYEMX6CIDFkrvpJwAKgJwEON1FBgqAnAQ43YclAzGmviXzU1IkovrKHl+sl8LQApg0m9MVh0+wf0cawXS88LMW+6lPv6cG6U4AGJjzwF9rX/1doxqDgr8TMX92stXoac/7xeVSBxhz14Uj989tKPp3Ikgdf2xj/PzsafP1K8pX/OSzUgYYmTDZF7t35mwteMiH7mv3CHLzi28aPB2LLZcu1srpXIoAw1PrO+Lu2iN1W155iub+Tg2QZrI70UYA5LzqAqAAyEmA011koADISYDTXWSgAMhJgNNdZKAAyEmA0/1/i2KajZYFbPgAAAAASUVORK5CYII=" width="80" height="80">
				</div>
				<div class="pure-u-3-4">
					<div ng-style="showIfLogged" class="content-box">
						<H5>Logged in as, <strong>{{user.gci_user_email_address}}</strong></H5>
						<button ng-click="Shared.route('site/listmail')" class="pure-button pure-button secondary-button">
							List Mail
						</button>
						<button ng-click="Shared.route('site/main')" class="pure-button pure-button-primary">
							Change User	
						</button>
					</div>
					<div ng-style="showIfNotLogged" class="content-box">
					</div>
				</div>
			</div>
		</div> 
	</div>
	<div class="container" id="container">			
        	<?= $content ?>
	</div>
<div class="footer" ng-controller="gciFooterController">
    <div class="container">
        <p class="pull-left">&copy; {{companyName}} {{date}}</div>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
