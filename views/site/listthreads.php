<div ng-controller="gciNotesController">
  <h2>{{title}}</h2>
  <p>{{description}}</p>
  <div class="no-msg box" ng-style="showIfNoThreads">
      No Threads Listed
  </div>
  <table class="pure-table" ng-style="showIfThreads">
	  <thead>
		<th>GMail Thread Title</th>
		<th>GMail Thread Date</th>
		<th>--</th>
 	  </thead>
	 <tbody>
		<tr ng-repeat="thread in threads">
			<td><a class='blue' ng-click='goToMessages(thread)'>{{thread.gci_thread_title}}</a></td>
			<td>{{thread.gci_thread_date_created}}</td>
			<td>
				<button class="pure-button pure-button-primary pure-button-default" ng-click="goToNotes(thread)">
					View Notes
				</button>
			</td>
		</tr>
	</tbody>
 </table>
 <div ng-style="showIfPages">
	 <ul class="pages">
		<li ng-repeat="page in pages" ng-click="goToPage(page)">
			<button class="pure-button pure-button-primary">{{page}}</button>
		</li>
	 </ul>
  </div>
</div>
