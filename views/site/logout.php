<div class="gci-logout content-box" ng-controller="gciLogoutController">
   <div ng-style="showIfLoggedOut" class="no-msg box">
		You have been logged out
		<button ng-click="Shared.route('site/home')">Home</button>
	</div>
   <div ng-style="showIfNotLoggedOut" class="no-msg box">
		Unable to logout.. Please contact support as this message means something is improperly configured
	</div>
</div>

