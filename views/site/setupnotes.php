<div class="gci-setup-notes form-box" ng-controller="gciSetupNotesController">
  <div class="pure-g">
     <div class="pure-u-3-4">
	  <div class="pure-form pure-form-stacked">
	     <legend>Notes for {{thread.gci_thread_title}}</legend>
	     <fieldset>
	         <label>Notes</label>
		 <div text-angular ta-text-editor-class="ta-border" ta-html-editor-class="ta-border" ng-model="thread.gci_thread_notes"></div>
	 	 <hr></hr>
		 <button class="pure-button pure-button-primary pure-button-default" ng-click="update()">Update Notes</button>
	     </fieldset>
	  </div>
      </div>
     <div  class="pure-u-1-4">
	 <p>{{description}}</p>
     </div>
   </div>
</div>
     
	   
		



