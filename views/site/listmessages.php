<div class="gci-list-messages form-box"  ng-controller="gciNotesMessageController">
  <h2>{{thread.gci_thread_title}} Messages</h2>
 <ul class="gci-messages-list list">
	<li ng-repeat="message in messages">
		<ol class="message-menu">
			<button class="pure-button pure-button-primary" ng-click="replyTo(message)">Reply</button>
		</ol>
		<label>
		  <strong>Subject: </strong> 
		  {{message.gmail.subject}}
		</label>
		<label>
		  <strong>To: </strong>
		  {{message.gmail.to}}
		</label>
		<label>
		  <strong>CC: </strong>
		  {{message.gmail.cc}}
		</label>
		<hr></hr>
		  <div ng-bind-html="getMessage(message.gmail.message)"></div>
	</li>
 </ul>
    <div ng-style="showIfPages">
		<ul class="pagination">
			<li ng-repeat="page in pages" ng-click="goToPage(page)">
				{{page}}
			</li>
		</ul>
	 </div>
				
</div>
