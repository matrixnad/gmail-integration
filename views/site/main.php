
<div class="gci-main" ng-controller="gciMainController">
  <h5>Global Options</h5>
		<ul class="ui-buttons">
			<li ng-repeat="link in navLinks" class="pure-u-1-10">
				<button class="img-button pure-button pure-button-default pure-button-primary" ng-click="Shared.route(link.link)">
					<img src="{{link.img}}"></img>
					<div class="content">
						{{link.title}}
					</div>
				</button>
			</li>
		</ul>
  <hr></hr>
  <h2>Select A User</h2>
	<table class="pure-table user-list">
	   <thead>
		<th>Email Address</th>
		<th>Full Name</th>
		<th>--</th>
	   </thead>
	   <tbody>
		<tr ng-repeat="user in users">
			<td>{{user.gci_user_email_address}}</td>
			<td>{{user.gci_user_fullname}}</td>
			<td>
				<button ng-click="switchUser(user)" class="img-button pure-button pure-button-primary pure-button-default secondary-button">
					<img src="{{userImgButton}}"></img>
					Use Account
				</button>
			</td>
		</tr>
	    </tbody>
	 </table>
</div>
				



