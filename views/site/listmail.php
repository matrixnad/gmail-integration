<div class="gci-listmail content-box" ng-controller="gciListmailController">
   <div ng-style="showIfNotAuthenticated"> 
	<div class="no-msg box yellow-box content-margin-box">
		<p><b>Important: </b> You need to authenticate this user to send email<p>
	</div>
   </div>
   <ul class="button-list">
	 <li ng-repeat="item in userItems">
		<button ng-click="Shared.route(item.link)" class="pure-button pure-button-default secondary-button img-button">
			<img src="{{item.img}}"></img>
			<div class="content">{{item.title}}</div>
		</button>
	  </li>
	 <div class="clear"></div>
   </ul>
		
  <hr></hr> 
  <button class="pure-button pure-button-default pure-button-primary img-button" ng-click="Shared.route(composeButton.link)">
	 <img src="{{composeButton.img}}"></img>
	 <div class="content">{{composeButton.title}}</div>
  </button>
	
  <div class="form-box">
	  <h2>Autoresponder For {{user.gci_user_email_address}}</h2>
		   <div class="pure-g">
			<div class="pure-u-1">
				 <b>Subject: </b>
				<br></br>
				<p>
				{{autoresponder.gci_autoresponder_subject}}
				</p>
			</div>
			<div class="pure-u-1">
				 <b>Message: </b>
				 <br></br>
				<p>
				{{ autoresponder.gci_autoresponder_message }}
				</p>
			</div>
			<div class="pure-u-1">
				<b>Signature: </b>
				<br></br>
				<p>
				{{user.gci_user_signature}}
			  	</p>
			</div>
		
			  <div class="pure-u-1 status">
			       <h5>Autoresponder Status</h5>
				<div  ng-style="showIfAutoResponderOn" class="status green-circle" ng-click="toggleAutoresponder()">
						<label>On</label> 
			<img class="icon icons8-Toggle-On" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAIEklEQVR4Xu2b708cRRjHn7nb2+OOOzigcFDbgoX+SDRSXrQIrS3VJm1pTWnS+q5pXxmjRvtKjb6hicYUTKxv1aSaGJv4I9K/oKRo+sakGOMrjeVHCy3lx3G3x/3au3FmYY/l2N3bu9njaJ1NyAE7z8zsZ78zzzPPzCHgFxMBxGTNjYEDZBQBB8gBMhJgNOcK5AAZCTCacwVygIwEGM25AjlARgKM5lyBHCAjAUZzrkAOkJEAozlXIAfISIDRnCuQA2QkwGjOFcgB6hO4c3ehBacSZzBGPRjhAAIUICX3rZQexYBDCKMQQngYudw3uzpqxophyazAgd9690EaVxfTuBWbRSnWmIrjoJWydd7tkfbA24c9qPGQgDxtVmw0ZUbJBseQVxC/6OioCVm1LRjg4O3eMxigjxj2kQbpWy3ZFU/IMP0gDJmMeRPIgaC5th26Gt4HAXmZ+kOejagSrlkFaQng57d6ArLgfRcwXC41NPXp0+kMTE1FIJlImwKpqQzC0W2fQKVQr1suFk/DUlxW7qVSacikMbgrBOVvL/n0VDgN6seLZIhf6u4MDpl1IC/Az349cRFnHNc2Cpza2ZlHEkQiSVN4rQ0HoLv+o3VlwlIKlmIpkKIyUS/RVJ7L5xWgJlChDxNB/8H9wStGVRgCpKpLCZ7rZPLty9cBu+9L0QQ8mo6aVvt420noqH1jTRmqttmFGMRi5qo1qrix3gtVfte62xlIfu9z+d7Smxt1AX5650SLU3b8Qm6qXstuRob10aF7/34Y5JTxxJcLj6psZi4G4Uiq6H4KTgQ7m6sM7VNY+qmns/V8boF1AJX5zum9t9FDVu3Y/HwMFsiP0ZU7bCm8yakoJJLFqU5tx0h92n7IeOnqkc5nP9D+bw3A5WHrvVUO5dFOUfVNjIcMvS51GKdbvsz2n8L7dyJiaZ4zk2YFcSQ7tvosqRdhOKt1LGsADtzuvU5c+CVLNZWgkJn6aKhydtdXWW9rl/LoY2wn8Iy98doHTROt+13+oDofZgEOjpzoAXDcKgEXy1WOj4VAlvXnvpYtHfBSsD9b18PHS0xznlpRIepTbaT02LfHuzoVoWUBDoz03i3X0KUdiZGwY+pBRBe2IDjh/O7vskEy9baTU5LlF2NWsBD1qfWkcVz2i1X1VIUKQDJ0L5Ghe92WHhVZyePZKIRDCV3rXPVNTktFhyraBopRn2ofkv+6ear7ZboiIwBHTg6TeO9Ikc9ui9nkRAiSSf3he7ytHxrcHUo7NFShw9eOqxj1qe3G8UPplc52P1pZpi3Y0aFi66Ded+ye/vrdLYrw2q4fs1VPzyxBhKw0WK9C1ZeSMXEfMvi8q4F22PlnO9oMw9ds/muq3Q3HmgazvP4Zo8mF/MuzfICtqI+2Iy3JIElJ5dNBIoG2ltVgezbz+w1EsitDxJWcyddgKe+HQjGYm9UPnvdvuwB7q88pzdvlPPKpj66lVWi5z60FH8n8PY02w/xnFv91N78Orb5TynPMzcdhzsDRFPKC9dSnKC1KlJYnAVEXcENdbYXSXBSPhwjA8oYvtCNmAE/vuQo1wl7bAGrVR5d/C4uJvNC0L0cLMIanYmhwpJd9Qink9euUfUCSB/GVnF3u7QvP3bTVgTRs8RBvn4boUgqoYyj08vtc0NSwmrT9XwCkmRYfeXDR5YQZg7nWKshAtQgNdZ5scQpwlPzVbrWCUpQzHcK7B6DGtafgIUw9Js3tVftFcIvLWWc7AvA1QzgzHXuqnAiFVkmyy3SYaeM1Oz04nQICVeKqE3kawhgFWqWom01WR4wd6qN1rQ9jNsE6uJhAWoXmqxSUANfssit+1A2kN/tSrsLthvNtP2T5SMR7ukUBXELe/bCsjV3qy/XAylKOtrIZHIl5MuEKSSYUtz1jl/oop62N3uzcGss8ko69+IL/iUxnFRIJ2KW+3OXfmnTWigrHyGdzIZ2zs6zZPOgi8du5XasJVavt2qk+rfMgaX2ZpPVXE6rLAJ+slL4ViHapL3fu003pKxDLnJkxC6gdZDXR17a6qZQPIHU2Uw/ZE6+i6ICWbf5sc2STPUk22ddvKtESKx55mPxalpUJTayOk21NbLCnHvDVw6vNX+djp9y/NxEuaq2rrZyGLTt3+NeESabbmtRYOZWQQqMIoZIdWTMjUOjGul5ddqT9qfLoXrE2xsy7sa52hp75Qxn4phxKVI52TJKjHQbbm7SPeuditCBZ1Zc759G6Sabw56MHWpczu5or3+GioXJsNhV7uIg+F4v6aKhSX+tZt8mexokbftH/puXDRVrCJMjuxxhf3ughbeV4W1tDJ3TVf7huFNPwZT4UV7YA8u2fqAmISo9Lfy2N4crBTs2OvlUFasutOJfLGwlSOWBJTqcabXWq/avxNcLRZz42PWBJM89pMiUkV6YFUXCAk/zQNJfJAcswOWB5kfmAZe7rvXr7ZJ+DHO9VjvmW2NHQE6WTk4uGXlntGw1xdgT2QVfwPeYjvqRO0iA54iuK16yclba+IjdwncsOJ1Oys9LhxXhjIpFuNPPc6r0tvh3h9qp3DrsdwUMu5Gm1YqOWwRj+IDCGrIJT7ZgBFtLJjSxLv+aQlhN9KIN6gHzNgXzdIUCOryjxrQILYRJwohB24GGn4B4q29ccNhLKZmzrqVXgRsHmABlJc4AcICMBRnOuQA6QkQCjOVcgB8hIgNGcK5ADZCTAaM4VyAEyEmA05wrkABkJMJpzBXKAjAQYzbkCOUBGAozmXIGMAP8DhWWfUFbIjSMAAAAASUVORK5CYII=" width="42" height="42"></img>
				</div>
				<div ng-style="showIfAutoResponderOff" class="status red-circle" ng-click="toggleAutoresponder()">
						<label>Off</label>
			<!-- Toggle Off icon by Icons8 -->
			<img class="icon icons8-Toggle-Off" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAH/klEQVR4Xu2bW2wUZRTHz+xu99Ztu0vp9kIpBfHywi2aGJ+E8EY0QOKDyoPUF4mRUI2RB2MgMUZeJAWDJioJCDbeolx8UFQo2KJUS9tYIglYkJrSrd122253t93LeM7QKdPt7OzMfkN3G74vaRronO/y2//3nfOdOSsAb0wEBCZrbgwcIKMIOEAOkJEAozlXIAfISIDRnCuQA2QkwGjOFcgBMhJgNOcK5AAZCTCacwVygIwEGM25AjlARgKM5lyBHCAjAUZzrsB8Afy1c6RejE9uFkVhvSiIXgEEL85l7fR8ukQQQ4IohARBbBGKHCefWOe7yTjXgjQ3pMDOzhFvJDG1C0TYooCla2HW2Ph1Z7C3ddnHOy4kA70leowcFgh4nDCg59mcnknCqHARunKynTbSBVAGJ4rQiAaktJybNToKVR9uB/HKRUiJ2t1YcLBaRG2z5jycXsMQPniCfoRf4KReI3ouK8C29kFcbaoJBKFMreNoLAmRWEL6UzyehFRSBIfTJv3bjb9dTvXV24b6oHrfJogPD2rO124BqEGIFvw9Ty2EO6wJbHBAaAECq9k0Abb9HtiDne1N72EsHIdINA7hiQSksskIjT1uG/i8TlWYS95/FpKd5zQnWWIHqCjOthTT/x6CFO64Njiq1bMqQNqysejooZTN9bzSmNQ2NBKFaDSZ02yrKtxQWlI0x9Z/7FWwnGvW7LPSDVDsyGlYNiMRt7YNGjKpURXgpZarXyXcvmfkkUllg8EojI3Hc56MzSrAimWlGe2zQbTReYjm87iV785VREeTgK3Cb3AzfQFzAF46f21fwlW6Wwmvr38CJqdyU53cTyb1KSe05CBu567M29nnBPC5cv4MWQ1DYIXl6UqcBbD9bM/LcU/FISW83lvjus45rdk50ZHU1Xh0LWDp66syOhbyynX5UiHNHpUotMI65UJmAErn3kQokLK78cgGCZoZyqO+liK8TN44nSp5Z//uxzKGOHlWIU33CIY6DfK8ZwB2f33mSLhuzQvyHwb+izCdeXI/RtQn2yx5byskMU5Ua3QW1jFForo2gvZDImxAJbbQQxJASX3jw0Mpp0cK2sjb9vWHTRjJmPrkASnYrt75MCQyhEgUXNvvhJr5aSK0IMANMwCvfPrlidAjT26WZ9N3O5xzqKJcUS7qk+1r3t0EqWsdqoDK8JApn/+4MH0uDbiVj0gKvPzdxfGo/wHplKdQhbavGc3I2Zc+XknPT1C8f5vqNOh2Uqt6LzJj1jr7EOE8qnC9cLW5eXVw5cZu2ez2YATG8abB2oyqL54QMVRK4K3lbqC99KVavB6qz6UeAeYlJlSCsYJP+Ovw4ebhVU89J///9ZtjzGEL9aVHfeTpw5EEhMNT0m8Lxikr6+8G27Vvb4TEjR7Vz5KiIufcSw3r527UvkH484tT/WPLH68mS7OcRzb10V1ahpY+YyX4xZ+/CbYzn6guqhwD6jIMrPPcTgpd3/w4MlG7WgoMgsMxCIYmmeekpj5JaROotCwJiHKvA8oX3SHjO/sROI6/pTqfAogHKbA+L1w+3RqJVj4oXZDMAKhUH13/RkYns0JTElIC1HIkBQKwS2hrD8ykNc1wIP7FLphCcBOROJBjMNpKPEVQ7cfUy3SrerFStQvyNVX6bodGp2DoeVMAUqbFgwu3F1lhcChqaALpD3sxyPPTAbdQAHacbovEKlca3sLkMSm3V4bZTof9TtbZjABcuYU9PT+DZ/+slOQM2ALZwt2GnAhBK8bsMm0zZbxGqzLLg9MR4C2V8hkLw4noCWMkaMV21WyyLAkz1Ed9KT14+bfvQNHpg6pHQsGEMZkCaRmap9gmBbhazSz1LchAOv0qF0bv6cBURxHljXQ2s9SX7oEXxFWOGCkdiU5mM4+ZpT7qsKbKPXO2ajmQgkom0MTT01lGIJqlvvTr34JKZ1FCdTI8HEw6PIZeX5upPqXzsMbGoPqVhwo5oSqlskhoGVP6elRolvrSz74Fl9InWNMvlQbxpZKuJBE5m/4B9sSrHQ+0esrRTzdb8F/wv/FoIb9UOoqZ6O3yfGe52o4fOrbFfLXH9ajvxq2xnO66yr4pbFlRVzIrTNJ8rYnGdflMpIrQjVtXLuGTljInVvnj+86myUU1u7QgmpH2J+XRu2JljFnQL9ZF+AdLPNZqvliXobX/1H0qXlr1dCaIrOpLP/NoHP+x17A+5rOMn1ueSzu6sbRji67SDvk8jEZGPxCLXDOpfnllLOqjUKVikWvOS/Zs8GjsvBUXAdYLWmG7oeIiGVbbpcBe3OR70mVB4ctwKCYlELKVt8kJiGJXkepdOtu2pbExdwH++c/9jeLQjfTqUus4y3pfmy6wPIAFlqqlVQSRMs/JRAqm8Iea3WYBK/5QmitjgSV62yossEwEA5o+Kw8FlqPTBZZNzAWW8sqkEt+pqUZUYyP+H9MbWSlMOboTrz86SnxxMCppu+clvuQgBKlUg0p8qdRXd8uqQGVPMkhM1G8RBFijexR42BIb/9s92Ntae3jHBQj0Zi4UVHSK6hu4p0XmKQjNS5G5Gij6mkMyMblFSAnrAb/mgF938MpQsRi9G7/egLXGQki0iC1Wm+ME/5qDEbndR88a2sL3ERfdS+UAdaNSf5AD5AAZCTCacwVygIwEGM25AjlARgKM5lyBHCAjAUZzrkAOkJEAozlXIAfISIDRnCuQA2QkwGjOFcgBMhJgNOcKZAT4P8HROK2p++yTAAAAAElFTkSuQmCC" width="42" height="42"></img>
				</div>
			  </div>
	     	   </div>
	   	</div>
</div>
