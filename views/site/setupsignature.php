<div class="gci-setupsignature form-box" ng-controller="gciSetupSignatureController">

   <div class="pure-g">
	 <div class="pure-u-2-3">
		<div class="pure-form pure-form-stacked">
			<legend>Edit {{user.gci_user_email_address}}'s Signature</legend>
			<fieldset>
			   <label>{{user.gci_user_fullname}}'s Signature</label>
			   <div text-angular ta-text-editor-class="ta-border" ta-html-editor-class="ta-border" class="pure-input" ng-model="user.gci_user_signature"></div>
			</fieldset>
			<hr></hr>
			<button class="pure-button success-button" ng-click="update()">Update</button>
		</div>
	 </div>
	<div class="pure-u-1-3">
		<p>
			{{description}}
		</p>
	</div>
   </div>
</div>
