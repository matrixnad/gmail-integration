<div ng-controller="gciSendMailController" class="gci-send-mail form-box">
      <div class="pure-g">
	 <div class="pure-u-3-4">
		<div class="pure-form">
         	<legend>Compose New Message</legend>

		<label>To</label>
		<input type="text" ng-model="mail.to"></input>
	        <label>Subject</label>
	        <input ng-model="mail.subject"></input>
		<label>Message</label>
		<div class="pure-u-1 email-editor" text-angular ng-model="mail.message" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
		<hr></hr>
		 <button ng-click="send()" class="pure-button pure-button-primary pure-button-default img-button">
			Send Message
		</button>
	    </div>
        </div>
        <div class="pure-u-1-4">
		<h5>Canned Replies</h5>
			<ul class="canned-replies scroll-box">
				<li ng-repeat="reply in replies">
					<label><h4>Name</h4></label> 
					<p>{{reply.gci_cannedreply_name}}</p>
					<!--
					<label><strong>Subject</strong></label>
					<p>{{Shared.textSnippet(reply.gci_cannedreply_subject,12)}}</p>
					<label><strong>Message</strong>
					<p>{{Shared.textSnippet(reply.gci_cannedreply_message,156)}}</p>
					-->
					<hr></hr>
					<button class="pure-button pure-button-default pure-button-primary" ng-click="useReply(reply)">Use Canned Reply</button>
				</li>
			</ul>
	 </div>
     </div>
</div>


