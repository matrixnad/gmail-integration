<div ng-controller="gciHomeController" class="gci-home">
  <h2>GMail Autoresponder</h2>
  <p>{{description}}</p>
	  <ul class="button-list">
		<li ng-repeat="item in buttonItems">
	  		<button ng-click="Shared.route(item.link)" class="pure-button pure-button-primary">
				<div class="content">{{item.title}}</div>
			</button>
		</li>
		<div class="clear"></div>		
	 </ul>
</div>
