<div class="gci-canned-replies form-box" ng-controller="gciCannedRepliesController">
   <h2>New Canned Reply</h2>
    <div class="pure-g">
	    <div class="pure-u-2-3">
		   <div class="pure-form pure-form-stacked">
			<fieldset>
				<label>Canned Reply Name</label>
				<input type="text" ng-model="cannedreply.gci_cannedreply_name"></input>
				<label>Canned Reply Subject</label>
				<input type="text" ng-model="cannedreply.gci_cannedreply_subject"></input>
				<label>Canned Reply Message</label>
				<div text-angular ta-text-editor-class="ta-border" ta-html-editor-class="ta-border" ng-model="cannedreply.gci_cannedreply_message"></div>
			 <hr></hr>
			 <button class="pure-button pure-button-default success-button" ng-click="save()">Save</button>
			</fieldset>
	           </div>
	   </div>
	   <div class="pure-u-1-3">
		 <p>
		    {{description}}
		 </p>
	  </div>
    </div>
</div>
