<div class="gci-setupmail form-box" ng-controller="gciSetupMailController">
 <div class="pure-g">
	 <div class="pure-u-2-3">
		<div class="pure-form pure-form-stacked">
			<fieldset> 

			<label for="subject">Auto Responder Subject</label>
			<input id="subject"type="text" ng-model="autoresponder.gci_autoresponder_subject"></input>
			<label for="message">Auto Responder Message</label>
			<div text-angular ta-text-editor-class="ta-border" ta-html-editor-class="ta-border"  id="message" ng-model="autoresponder.gci_autoresponder_message"></div>
			<label for="active">Auto Responder Active</label>
			<input id="active"type="checkbox" ng-true-value="1" ng-false-value="0" ng-model="autoresponder.gci_autoresponder_active"></input>
			<hr></hr>
			<button class="pure-button pure-button-primary pure-button-default" ng-click="update()">Update</button>
			</fieldset>
		</div>
	</div>
	<div class="pure-u-1-3">
		<H2>Canned Replies</H2>
		<div ng-style="showIfCannedReplies">
			<p>{{subdescription}}</p>
			<ul class="canned-replies scroll-box">
				<li ng-repeat="reply in replies">
					<label><h4>Name</h4></label> 
					<p>{{reply.gci_cannedreply_name}}</p>
					<!--
					<label><strong>Subject</strong></label>
					<p>{{Shared.textSnippet(reply.gci_cannedreply_subject,12)}}</p>
					<label><strong>Message</strong>
					<p>{{Shared.textSnippet(reply.gci_cannedreply_message,156)}}</p>
					-->
					<hr></hr>
					<button class="pure-button pure-button-default pure-button-primary" ng-click="useReply(reply)">Use Canned Reply</button>
				</li>
			</ul>
		</div>
		<div ng-style="showIfNoCannedReplies">
			 <div class="no-msg box">
				<small>You have not setup canned replies</small>
			</div>
		</div>
	 </div>
 </div>
</div>

