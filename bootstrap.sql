
CREATE TABLE `gci_users` (
    `id` smallint(3) AUTO_INCREMENT,
    `gci_user_email_address` varchar(255),
    `gci_user_password` varchar(255),
    `gci_user_gmail_auth` varchar(2555),
    `gci_user_signature` varchar(255),
     `gci_user_fullname` varchar(255),
    PRIMARY KEY(`id`)
);

CREATE TABLE `gci_master_accounts` (
   `id` smallint(3) AUTO_INCREMENT,
   `gci_master_account_username` varchar(255),
   `gci_master_account_password` varchar(255),
   PRIMARY KEY(`id`));


CREATE TABLE `gci_autoresponders` (
   `id` smallint(3) AUTO_INCREMENT,
   `gci_autoresponder_subject` varchar(255),
   `gci_autoresponder_message` varchar(255),
   `gci_autoresponder_notes` varchar(500),
   `gci_autoresponder_active` smallint(3),
   `gci_user_id` smallint(3),
   PRIMARY KEY(`id`)
);


CREATE TABLE `gci_cannedreplies` (
   `id` smallint(3) AUTO_INCREMENT,
   `gci_cannedreply_name` varchar(255),
   `gci_cannedreply_subject` varchar(255),
   `gci_cannedreply_message` varchar(255),
   PRIMARY KEY(`id`)
);

CREATE TABLE `gci_threads` (
   `id` smallint(3) AUTO_INCREMENT,    
   `gci_thread_title` varchar(255),
   `gci_thread_uid`  varchar(255),
   `gci_thread_notes` varchar(255),
   `gci_thread_email` varchar(255),
   `gci_thread_to` varchar(255),
   `gci_user_id` smallint(3),
   `gci_thread_date_created` datetime,
   `gci_thread_date_updated` datetime,
    PRIMARY KEY(`id`)
);
CREATE TABLE `gci_threads_messages` (
   `id` smallint(3) AUTO_INCREMENT,
   `gci_message_uid` varchar(255),
   `gci_thread_uid` varchar(255),
   PRIMARY KEY(`id`)
);


-- THREAD ID is unique to the provider
CREATE TABLE `gci_notes` (
   `id` smallint(3) AUTO_INCREMENT,
   `gci_thread_id` varchar(255),
   `gci_message_id` varchar(255),
   `gci_notes_note` varchar(255),
   PRIMARY KEY(`id`)
);

CREATE TABLE `gci_logs` (
   `id` smallint(3) AUTO_INCREMENT,
   `gci_user_id` smallint(3),
   `gci_log_message` varchar(255),
   `gci_log_details` varchar(255),
   `gci_log_date_created` datetime,
   PRIMARY KEY (`id`)
);



INSERT INTO `gci_users` (`gci_user_email_address`, `gci_user_password`, `gci_user_gmail_auth`, `gci_user_signature`, `gci_user_fullname`)
VALUES 
("deborah@cabinfield.com", "deb#@4!9", "", "", "Deborah"),
("david@cabinfield.com", "r2Jlnzth", "", "", "David"),
( "sales@cabinfield.com", "TestingAPassword", "", "", "Testing Fullname"),
 ( "support@cabinfield.com", "TestingAnotherPassword", "", "", "Testing Fullname"),
 ( "matrix.nad@gmail.com", "101010", "", "", "Nadir Hamid")
;

INSERT INTO  `gci_master_accounts` (`gci_master_account_username`, `gci_master_account_password` ) VALUES ("MasterAccount", "MasterPassword");
INSERT INTO `gci_autoresponders` (`gci_user_id`, `gci_autoresponder_subject`, `gci_autoresponder_message`, `gci_autoresponder_notes`, `gci_autoresponder_active`) 
		VALUES (1, "Testing An AutoResponder", "Testing An AutoResponder Message", "Testing My AutoResponse Notes", 1),
		(2, "Testing Second AutoResponder", "Testing Second AutoResponder", "Testing Autoresponse Notes", 1),
		(3, "Testing Another AutoResponder",  "Testing Third AutoResponder", "Testing Autoresponse Notes #2", 1),
		(4, "Testing Another AutoResponder", "Testing Fourth AutoResponder", "Testing Fourth Auto Responder #3",1),
		(5, "Testing Another AutoResponse", "Testing Fifth AutoResponder", "Testing Fifth Auto Responder", 1)
		;

INSERT INTO `gci_cannedreplies` (`gci_cannedreply_subject`, `gci_cannedreply_message`, `gci_cannedreply_name`) VALUES ("Testing A Subject (Canned Reply)", "Testing Email (Canned Reply)", "Canned Reply #1"),
		("Testing Another Subject", "Testing Email For Canned Reply", "Canned Reply #2");
INSERT INTO `gci_threads` (`id`, `gci_user_id`, `gci_thread_uid`, `gci_thread_date_created`, `gci_thread_title`) VALUES 
		( NULL, 1, "T-XXX", "2016-05-12", "Testing A Thread" ),
		( NULL, 1, "T-XXX-1", "2016-05-12", "Testing Another Thread" ),
		( NULL, 2, "T-XXX-2", "2016-05-12", "Testing Another Thread second user")
		;








 

