<?php

namespace app\components;

final class MIMEComponents {
 	public function getSender($user,$message,$lines) {
		//$lines = $this->getMimeLines($decoded);
		foreach($lines as $lineName => $lineValue) {
			if ( ((strtolower($lineName) == "from" ) && $user->gci_user_email_address !== $lineValue)
			|| (strtolower($lineName) == "to") && $user->gci_user_email_address !== $lineValue) {
			     // message should be in <user@email.com>
			    $this->logData("Reading Sender: ", $lineValue);	
			    $emailAddress = preg_match("/\<(.*)\>/", $lineValue, $matches );
			    if  ( $matches ) {
			    	return $matches[1];
			    }
			 }
		}
	 	return false;
	}
	public function getSubject($message,$lines) {
		 foreach($lines as $lineName => $lineValue) {
			 if (strtolower($lineName) == "subject") {
			     return $lineValue;
			  }
		   }
		 return false;
	 }
  	public function getKey($lines,  $key) {
		foreach($lines as $lineName => $lineValue) {
			if (strtolower($lineName)==strtolower($key)) {
				return $lineValue;
			}
		}
		 return false;
  	 }

   	public function getMimeLines($decoded) {	
	  	 $lines = explode("\r\n",$decoded);
		$mimeKeyPair = [];
		 foreach($lines as $line) {
			$split = explode(":", $line);
			if ( !empty($split) && sizeof($split)>1) {
				if ( sizeof($split)>2) {
					$value=implode(":",  array_slice($split,1,sizeof($split)-1));
				}  else {
					$value=$split[1];
				}

				$mimeKeyPair[$split[0]]=str_replace("\n", "", str_replace("\r\n", "", $value));
			  }
		  }
		//$this->logData("MIME Headers", json_encode($mimeKeyPair));
		 return $mimeKeyPair;
	 }
}

