<?php

namespace app\components;
require_once(__DIR__."/../vendor/autoload.php");
use Yii;
function base64url_encode($data) { 
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 
function base64url_decode($data) { 
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
}

final class GmailComponents {
	public function  getAuthUrl() {
		$client = $this->getClient();
		$url = $client->createAuthUrl();
		return $url;
	 }
       public function getAuthorizedClient($user) {
		$client =  $this->getClient();
		if ( $user->gci_user_gmail_auth ) {
			$client->setAccessToken($user->gci_user_gmail_auth);
			if ( $client->isAccessTokenExpired() ) {
				$client->refreshToken($client->getRefreshToken());
				$client->setAccessToken( $client->getAccessToken() );
				$user->gci_user_gmail_auth = $client->getAccessToken();
				$user->update();
			}
			return $client;
		}
		return false;
  	}

       public function getClient() {
		$appComponents = new \app\components\ApplicationComponents;
		$config =$appComponents->getConfig();
		$client = new \Google_Client;
		$client->setApplicationName($config->GMAIL->APPLICATION_NAME);
		$client->setClientId($config->GMAIL->CLIENT_ID);
		$client->setClientSecret($config->GMAIL->CLIENT_SECRET);
		$client->setScopes($config->GMAIL->SCOPES);
		$client->setRedirectUri($config->SITE_BASE_URL."/?r=site/granttoken");
		$client->setAccessType('offline');
		return $client;
	}
	public function getAccessToken($code) {
		$client = $this->getClient();
		if ($client->authenticate($code)) {
			$accessToken = $client->getAccessToken();
			return $accessToken;
		 }
		return false;
 	}

	// this is always who we want to send to, not anything specific to To or From
	public function getEmail($user,$headers) {
		$email = '';
		$userEmail = $user->gci_user_email_address;
		foreach($headers as $header) {
			if (( (strtolower($header['name'])=="from" )
			&& strtolower($header['value']) 	!= strtolower($userEmail)) ||
				((strtolower($header['name'])=="to")
				&& strtolower($header['value']) != strtolower($userEmail)) )
			{
				$email= $header['value'];
			}
		}
		return $email;
	}
		
	public function getHeader($headers,$key) {
		$value='';
		foreach($headers as $header) {
		if(strtolower($header['name'])==strtolower($key)){
			return $header['value'];
			}
		 }
		return $value;
       }

   	public function getMessage($user, $gmail) {
		$payload=$gmail->getPayload();
		$parts = $payload->getParts();
		$messageArray  =[
			'messages' => []
		];
		$body = '';
		// use html if we have it otherwise use text/plain
		foreach($parts as $part) {
			$mimeType = $part->getMimeType();
			
			$decodedBody=base64url_decode($part->getBody()->getData());
			if (  preg_match('/^text\/plain/', $mimeType, $matches ) ) {
				$messageArray['messages']['text']=$decodedBody;
				$body = $decodedBody;
			} elseif ( preg_match('/^text\/html/', $mimeType, $matches ) ) {
				$messageArray['messages']['html']=$decodedBody;
				$body = $decodedBody;
			}
			
			$messageArray['messages'][$mimeType] = $decodedBody;
		}
		//$lines = Yii::$app->MIMEComponents->getMimeLines($raw);
		
		$lines =$payload->getHeaders();
		$email = $this->getEmail($user,$lines);
		$subject = $this->getHeader($lines,"subject");
		$to = $this->getHeader($lines,"to");
		$cc = $this->getHeader($lines,"cc");
		$bcc = $this->getHeader($lines, "bcc");
		$date =  $this->getHeader($lines,"date");
		$messageArray = array_merge($messageArray,[ 
		'to'=>$to,
		'cc'=>$cc,
		'bcc' => $bcc,
		'subject'=> $subject,
		'message' => $body,
		'email' => $email,
		'date' => $date
		]);
		return $messageArray;

	}
		
       public function getMessages($user, $messagesFull) {
	    $googleClient = $this->getAuthorizedClient($user);
	    if ( $googleClient ) {
		    $gmailClient = new \Google_Service_Gmail($googleClient);
				
		     foreach($messagesFull as $key => $message) {
			$gmail = $gmailClient->users_messages->get("me", $message['gci_message_uid'], ['format' => 'full']);
				
			if ( $gmail ) {
				$messagesFull[$key]['gmail']= $this->getMessage($user,$gmail);
			 }
		     }
		    return $messagesFull;
	    }
	    return false;
	}
	
		
       public function sendMail($user, $to, $cc='', $bcc='', $subject='', $message='') {
	    $googleClient = $this->getAuthorizedClient($user);
	    if ( $googleClient ) {
		    $gmailClient = new \Google_Service_Gmail($googleClient);
		    $newMessage = new \Google_Service_Gmail_Message;
		    $mimeMessage = new \Mail_mime;
		    $mimeMessage->addTo($to);
		    $mimeMessage->addCc($cc);
		    $mimeMessage->addBcc($bcc);
		    $mimeMessage->setSubject($subject);
		    $mimeMessage->setTXTBody($message);
		    $mimeMessage->setHTMLBody($message);
		    $message = base64url_encode($mimeMessage->getMessage());
		    $newMessage->setRaw( $message );
		    return $gmailClient->users_messages->send("me", $newMessage);
	    } 
	    return false;
	}
	
		
}


