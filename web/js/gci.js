
var gciApp = window.gciApp || {};

var baseUrl = function(extras) {
	var path = document.location.pathname.slice(0,document.location.pathname.lastIndexOf("/"));
	 return ("http://"+document.location.host+path+extras);
};

gciApp.defaults = {
	"baseUrl": "http://ec2-184-72-65-236.compute-1.amazonaws.com/gmail-integration/web/",
	"apiUrl": "http://ec2-184-72-65-236.compute-1.amazonaws.com/gmail-integration/web/?r=api/"
};
gciApp.statusCodes = {
	"OK": "OK",
	"ERROR": "ERROR"
};
gciApp.module=angular.module("gciApp", ['textAngular']);
gciApp.module.factory( "Shared", ['$http', '$window', '$timeout', function($http,$window,$timeout) {
	 var SharedData = {};
	 SharedData.onLoad = function(container, endpoint, data, successCallback, errorCallback) {
		 successCallback=successCallback || function(){};
		if ( endpoint ) { // directly call or no
		 SharedData.apiCall( endpoint, data, function(data) {
				successCallback(data);
				$("#"+container).show();
			}, function(data) {
				errorCallback(data);
				$("#"+container).show();
			});
		  } else {
			$("#"+container).show();
			successCallback();	
	 	 }
	  };
	 SharedData.apiCall = function(endpoint, data, successCallback, errorCallback) {
		var options = {"method": "POST", "url": gciApp.defaults.apiUrl+endpoint, "data": data};
		console.log(options);
		$http(options).then( function(response) {
		   if ( response.data.status === gciApp.statusCodes.OK ) {
				successCallback( response.data );
			} else {
				errorCallback( response.data );
			}
		});
			

	 };
		
	 SharedData.route = function(location) {
		$window.location.replace( gciApp.defaults.baseUrl+"/?r="+location );
	 };
	 SharedData.warn = function(message,type,after) {
		after=after||function(){};
		type = type || "error";
		$("#warn").show();
	 	$("#warn").find("."+type).text(message);
		$("#warn").find("."+type).show();
		$timeout(function() {
			$("#warn").hide();
			$("#warn").find("."+type).text("");
			$("#warn").find("."+type).hide();
			after();
		}, 2000);
	 };
	 SharedData.enumerate = function(upto) {
	   var pages = [];
		  for ( var i = 0 ; i <= upto; i ++ ) {
			pages.push( i  );
			}
		 return pages;
	 };
	 SharedData.getQueryParam = function(name) {
		var queryArray = URI(document.location.href).query(true);
		return queryArray[name];
	 };
	 SharedData.getId = function() {
		return SharedData.getQueryParam("id");
	 };
	 SharedData.textSnippet = function(text,amount) {
		var textResult = text.length>amount ? text.substring(0,amount): text;
	  	return textResult;
	 };
	 SharedData.checkPages = function($scope, data ) {
		if ( data.pages>0 ) {
			$scope.showIfPages['display'] = "block";
			return SharedData.enumerate( data.pages );
		} else {
		     	$scope.showIfPages['display']="none";
			return SharedData.enumerate( data.pages );
		}
	  };
		
	return SharedData;
}]);

gciApp.module.controller("gciHeaderController", ['Shared','$scope', function(Shared,$scope) {
	 $scope.applicationTitle = "GMail Integration";
	 $scope.showIfLogged = {"display": "none"};
	 $scope.showIfNotLogged = {"display": "block"};
	 $scope.userDynamicRoute = "site/home";
	 $scope.Shared=Shared;
	 
	 $scope.init = function() {
		$("#warn").css({
			"position": "absolute",
			"left": (jQuery(window).outerWidth() / 2) - (jQuery("#warn").outerWidth()/2),
			"top": (jQuery(window).outerHeight() / 2) - (jQuery("#warn").outerHeight()/2)
		});

		Shared.apiCall("user", {}, function( data ) {
			$scope.showIfLogged['display']="block";
			$scope.showIfNotLogged['display'] ="none";
			$scope.user = data.data;
			$(".header").show();
		 }, function() {
			$scope.showIfLogged['display'] ="none";
			$scope.showIfNotLogged['display'] = "block";
			$(".header").show();
		});
	 };
	 $scope.getUserRouteDynamic = function() {
		Shared.apiCall("user", {}, function() {
			Shared.route("site/main");
		}, function() {
			Shared.route("site/home");
		});
	 };
			
		

	$scope.init();
}]);
gciApp.module.controller("gciFooterController", ['Shared', '$scope', function(Shared, $scope) {
  	$scope.companyName = "GMail Integration";
	$scope.init = function() {
		$scope.date = (new Date()).getFullYear();
		$(".footer").show();
	  };
	 $scope.init();
}]);
		

	


gciApp.module.controller("gciMainController", ['Shared', '$scope', function(Shared, $scope) {
	$scope.Shared = Shared;
	 $scope.users = [];
	 $scope.navLinks = [{
		"title": "Canned Replies",
		"link": "site/setupcannedreply",
		"img": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAH1klEQVR4Xu2bW0xURxjHv8NyvwjesIqWXVkua0FZTGoaMdI0aZr0wZA0afvQlKSlNWlSfW1iGpveHtU+NVUr9qHpS7Mk9kVsC0QbL42sAgVkwV0UkIvcZJdlYXdP5zu4cA6c2zJnb3LmkTMz55vf+c98/xlmGdALFQGGqrXeGHSAlCLQAeoAKQlQNtcVqAOkJEDZXFegDpCSAGVzXYGxBmh3OvPAF/wQIFgHwFRSxhMPzRuJO260lpovqwmGSoH2bkcNMIyNvChPzcsSrE4LpBlqrSbTtFzc6wb4c+PVd4PBwOWKkr1pqcnJCcZGOdypWTd09bsmDx86UCQHcd0Av7vwq++Zey41b1MWvFphgc052cpRJUiN3oEh6OxzwuKiH/bs2N762fvHaqRCXxfAX65cO9XVP/A1v1OrpRhKCgsSBJF4mAt+P9zp6IGh0afLFTLT04JfHv/AoCnAP2/dPXv/wcMT41Mzgn7zt+TB4apySMQpPTY5DTfsnZzq+OWVokIoNxtfJwJpEYO4LgXae/oaSWfHOh1O+K9/QNBvSkoyVFvLAWEmSsEx4Fj4JTnZAEeqKpbGwbIaA+x2tJDsexRfiF/udnsPzM3PCwIoMRaAtaw4rhl6vD6iunaYfuYRxLkrfysc2m9ZmUmRBIhvxrXjdns3DI9NCALBBFNt3Q9ZGWlxB3Jw7Cm33q2espUWM5QW7hbGG2mAobc5h0ahrbsX/P7AcgA4pVGJpoIdcQERP/a97j5wDo0I4snNyeJUJ+omogUQI0L/hGqcmRVOi4Id24jdKYtpgsHY7nR0r5myxcQ9lBebpGOLJsDQZ23rdoCD+Cl+ycpI57J0LDwjejuyc1qTKFB1u/O3yc+OWADEiHCdQTXypzT+Hb822oNoFJyy/7R1csmOX7ZvziVTdp+69TlWAEMJ5sbdDoiFZ5T1duQjqi6xBBgKUsoz4rqoOIVUj3Slor3HAb0u4RKSmZ5OVFcWvkeNB4ChBHPjbmdEPaNqbxfOR4kXgKEpbe/qA9ew0EZocSiBNgqVx/d2uKOospTQ2ah4AqjkGcvNprAPJcQOAfA9st4uURXIjxun2/W2dirPiN4Os6zHK9xKorerIidEmpR4VCB/YFKeEROM3KGE1CGAKm8XDtl4B4hjQctxva1DlWeU83bVByu03+0kAsBQghE7lEAV4qk3HkpIHQJw53bheLsXTYH88TwYGOQ2/PyChxLoF1cfAqC3qz4Y4e1hoiiQD0zqUIJfx7jrJbDuM2s/ZVerMxEBynlGTbxdvE5hdp4Fz605yK7Jkg0x4A7A8G9jsOfjnYpD4R9KoLc7UhXlQ9poKdA/5oeZP2bBsCkJ8t7JlQQz99AL/d8/htT8FCj9Vt2mHj2jc3A4colC7jNGAyDCm/59BlgfCym7UyQBIrzeUy4IeAKQXZ6lGqCiTCNZIdIA57vmwd3q4eBhkQI48dcUPL44wsHDogMkEBDebJNbaDlEFIjwXD8Ij5c2PEB3iwe897xrJs9qBT6+8ATGrgj/Y7fhFfjsmvuor8snuvLwAbrODcLE3+IXnDakApvr7HlsYcDpH/VLXj1AgDlvZZNM+whmO4X/neMT33AAEd5imq+ZXECUvUhp2G6AkX8nwetcO72XAZJ8Y8g2QKYpXVX+zCEZG0vuoRzI3Juhqo1mlbTIwk31dypZQ9BGLtMY5QJbmF2Epz0z4HcLL+loNhjSUWp+Kux6Lx+2vhGl+ze0ABEeJAVRebIRI7wx+xQEA0EteUn2haos+uJlTskRLTQAm47frAOWOaMEz/PEC1OO2ajBCwHLMGVA6TfGyEJkDSarxeQS+0iy19s4eMBcUvq6CG+CTNtYFVRiicotYbgxssDeryorllzzJQE+TxguojzpTS2JJrgYhKGb48AGlnYhsSrGzwvImrhZ+9czUEtu7ON9SNEir0Bc+wwB0piRvYfBJY7OafDPr9zK0n4k8j1iYqk4X6L1a89Zy8wn5TpVvKGKSlxI87UwDByQ6wiVOGqfhEVP5LKvEh3LmSJNLA6ZSzMMCyetFnOD0jsVAWIH3HROn0clcrdSpQpCHCdK9E0vKL03Is/z394ysKd+p4ui8xYyRhekGxqVfh8SeocqgKHKV4/fbGCAIb9Kki/j7ZOj3omFqN+oZFn2q/re2tNK8Wn5PCyA+OKmT2+dJYnlhHwQbOuj5pF75GtK1yPzJMiy93GqyPZlILseFk4zDCObzLAPkjFb6x/U1mgJSKmvsAFyEBXtDdv65o+v1VwsttWxSdI2SO2Az5faThLln1EajNr+lPoJ5/m6AC5DZBlUo4gylgBiPYQYZOCsmILUDvi82VbDGJhmpYGp7U+pn3CerxsgB3Fpi0d+8rAa4gpArPdTsa2SZHGSyYXTUO2AX1iAyxDXeEUhwBDEpCQQeEod4HOtr/WKawFi1UtGW14gFfBHOpyn1AHyFguhVxQHGILoT4NGkhSO6gBFVtslrwjGUBKRWpAvlNgaWAaMamzHC70GigFCm0MANihlM8zQHzlqFettOIBK4MJ9rgMMl9iq+jpAHSAlAcrmugJ1gJQEKJvrCqQEiDsYf7L8P/O5nQ0D0584askxWvQK1WFC9MKM3zfpACm/jQ5QB0hJgLK5rkAdICUByua6AnWAlAQom+sK1AFSEqBsriuQEuD/Bpg7jQgWZL4AAAAASUVORK5CYII="
	 }];
	 $scope.userImgButton = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAIDElEQVR4Xu2cW0wUVxjHz8wOsMulrCgiWLmoXK2IityidlmuWm1iatM21aiNmvrUJk2TvtU+tLX1oT60aVNr1Ic2bZO2XhCXxV2IWFcJCohcRW4aBAUE3RuzszOdg2yzwR2Y6+6w7Dztwzlnz/c73/9833xnZhAQuAQRQAT1DnQGXgNYr9FkESi6DwsJ0VAkGUkSRBLkj2JYL4og4wSOn1eQ5Pmc2tqm+bQukgM0abX7UYXiGxpUuDo2VqGOiwtRYBhQRUZOcbJNTAAnQYDxwcHJ8UePnE6H4xlFUZ/lGwxn5wNIyQCaNJpEhUr1T4hSmRi3Zo06fPFiVjzMo6NgsLV13G6xdJM4/nZ+bW0fq44+aiQJQFNxsYYe+FJsenro0pUreZn2uKcHDLa32xQEUSBnWYsOcAoeguhX5eYGsfU6JsLQG3vq63EEx3PlClFUgFC2CIa1rsrLCxUKzwUVQuy+ccMGCCJDjnIWFWB9eXlTTHLyOr6yZfJEKOehzs6GXL1+E6/9QMJOogGE0ZaOrCdSt259EV4ZLjtiAwOhPWAUHQIERQAMwcBichmIt64ESkrF2K+9psZMB5a9BQbDOQl5cB5aPIAlJY9X5+ZGzybdCcUYaAtrAvFpaSBqSQxYFB0Dnj4ZBmPDQ2CgqwNkWNeDSGeURyOmpTycf+XKMs5WSthBFIAwSQYq1fW1paWMLgQ9rzHCBLILi0GE+mVIz8fHQIOxGqw3FzB6YotebwM2m6yisigAr2u1J5YkJBxZkZkZzLTYXeF3QVhGFFiVnsnoD/fvNgNL51OQYn7NY5uHLS2TI319x/KNxqMSOhWnoUUBCINHUnb2utnka4owgGxtiUfvc80YyrmprhbkPy9ilHFvQ0Nzjk6XxclKCRuLAvBmWVlvcn5+ouv2zNN868KrQMlb789pSvVfv4It5jKP7eBt3z2TqS+3qmrqPloOlygATUVFVNaOHbPaIwZA+AdNFRWAvk8WZd5iLIAoEwkAFLgUAQkLBMgmiIghYZgL+mUQYZPGiAHwwZ07+Gh//9f+l8awSKTFAOi3iTTcAegy1tDqvLwYplxQKEC/vpWbAkgXE0LV6u9StmxRS5EHthmNlkmrdY/fFhMgtJulpQ3LUlM3eipnCfHA6XLWLbqclS0w3oneXZQ80DUrWFAFGNZGS1k1U8p8AS6ogur0XuixpM8H4FRJ/+ZNHHE4FkZJ/39PpM9F6N+Vy9PTVdHTh0pcAS7YQyV3OSuUynNYcPAquswV3pjQwLqYsORisNlht3fSx5q75XgO4r6RiroHetqhYXQGKHrs+TtYDNtqTPjvjl1yi7ZM0UdygK4/1p8sp9gCLD2k89q8hIZlr000AFDgUnEFePnH8kSlKvg4QRDbKYoMRRDUimFYpd2Gf7rtiK7PNR227QROn7G7LD2QnlQhgqKXE9PWKN1P70YfD9n7u9ponuT2kkO62uqT5Ro27aSCB8eVJUAURe05hWVKptO7eqPO7ph0lAQpg6vnbOeg0qHHSuWpsgS4MmPtrKd33a3N9gf3O0fjk9OWz3bKN93uEv3I3PdSeaosAeYVbZvz9O52nQHkaMvnbHfrqtGGoABh66lc5S5LgGzTHTbtDOf+AImp6Ww8+pJ2/8XdAYAzCBj+/o2dp9YZrSUHK8MCAGcQgOfMbDwVtuOTwPu9hAMAGTTFBcyC8kCue5BY7f1GwmIB4TpOACBXYh6CjVeCSNUv5fuCsaDPHQ7H1BNSQUFBvTjh+KLsoO7sbDawLSYI5MC7u+QeWHNaoybJ8Bvq6OgVSWlrQhfRj+jC6+nIMOjtaLWOP3nyAEXNeYUHasc9WbHgARpO7ehI27ApNS7B84szA90doLe9ZYAin63zBPHKqTeool3v8vYQqTtK6oHVJ8s+joqJ+3LDZm3obIYM9veAe3dueYS4oD2w5sybPesKtia5ZMsH4oIGyNV4T57IdQypJTtzfEklzMf4mRD5jOFNiJICNJ7Z2ZtV8HoiGwm7G+0O0UEon7K5pfImNPf/khSg/ufy/YuXxf2wYXPhrEHEk/EuiDiOxy9YgBAMnYZ0Jq/NSolfncbZSSDE1gYTq7IS58FF6iCpB8I5wkQaoK/cScncuIIpF5wrOvPpJxKfOYeRHKAYEOe0wocNvALQnyF6DaC/QvQqQH+E6HWA/gbRJwD9CaLPAPoLRJ8C9AeIPgc43yHKAuB8higbgPMRos1iAaYrFaNFByqWcL0ZkuzRDqH3zlwNEdIeHow1m642F+67wPljFpIBnE+eCA/Eulsbfyo6UHmE60JICnC+QPy36oLZYpnYW3ZQz/mzUpIDlDtEuP9d112wFR+q5FwshrZ5BaCcId6+VmMdGR48XnZId5SrfL0KUI4QYfBovFbzpOiDS0v5wPM6QDlBhB87q6/RT5JOPK/0cDXvLwd7TcLuKzz1nA2IaIhLSHo1LWtTCN/V59sPwqPTljF6//uk9LDuDN9xfOKBrsm+gBj2Z9gr6vyNm7XhWBDjh9+E2PdSXyjb23U1tOc5PxQKz6cAXZYZTu88RpHOjxJTMpCElPQQqUDCaNveWG8dHxm2OAm8VIhs3VfFJxKe6RbwNSwsRPEV5XS+F7U0diI2ISlSpQqjX6JRA75AITC71QKeT4yBB/e7zHazReEE5Ld8oy2TDGQB0F3WuDNYE4Sp9iAItZp+RSuVpCglHw3TX0sfpd+5e0iShIlwElV8kmQ2/ysrgGwmLLc2AYACV+Q/0/oNnOo34GQAAAAASUVORK5CYII=";
	 $scope.init = function() {
	   	Shared.onLoad("container", "listusers", {}, function(data) {
			$scope.users = data.data;
		}, function(data) {
		 	Shared.warn(data.message);
		});
	 };
	 $scope.switchUser = function(user) {
		var userData = user;
		Shared.apiCall("switchuser",  userData, function(data) {
			Shared.route("site/listmail");
		}, function(data) {
			Shared.warn(data.message);
		});
	};
	$scope.init();
}]);

gciApp.module.controller("gciHomeController", ['Shared', '$scope', function(Shared, $scope) {
	  $scope.Shared = Shared;
	 $scope.description = "Setup autoresponders, canned replies and more";
	 $scope.buttonItems = [{
		"title": "Login",
		"link": "site/login",
		"img": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAACT0lEQVR4Xu3cP04CQRQG8KHWwAWspNIYSl1AShMrCo9hbeUZqCw8hoWJHsDoBdxFL2DhISS4E0OHC8z35s3OzEcL32b3t2+ZN/uvY/iBBDpQmmEjBnhz/7pM2XN2fb7WShRwejVO0vDx4c0QENi1BATwbJSABAQFwDgrkICgABhnBRIQFADjrEACggJgnBVIQFAAjLMCCQgKgPEoK7D6/DInRwfgpsvEowSc3T2by4tBKxCjBex298zorB8cMVrA4agw86oMjhgt4GQyNj+LRXDEqAHtMBAaMXrA0IhJAIZETAYwFGJSgCEQVQBfvpeidybYRtqOwv997MBSvtctTtE3g2O/M5YkAVeVqIGYLKAWYtKAGojJA/pGzALQJ6IT4PT2SXRUdTkz1zQKa47OzoAuG+ACJZ2RbnGyA5Q+nLMElETMFlAKMWtACUQC1vPmqizNsL6+4jJvzhrQjsgInq3gbAEl8LIFlMLLElASDwKUniHsujyXmZA0njPgrhurfUZ63fr5wMsG0BdeFoA+8ZIH9I2XNODfLR+VKU4PnWYY2/7POzXS2y589TvtQcTifcwrM6zxfN+IqQJon1iXfOC66bqwJp7aIawFqI2XFGAIvGQAQ+ElARgSL3rA0HhRA9qbzLValaa2Ldo2ptfbV+nzNvW80QLyQZtNu7bhez7qBeC1LRrlIdwmRAKCe4OABAQFwDgrkICgABhnBRIQFADjrEACggJgnBVIQFAAjLMCCQgKgHG1CgTXs9Vx728yb/XWe1w5sXfpe1zHVi/6FyZjTH5gETFTAAAAAElFTkSuQmCC"
		},
		{
		"title": "Learn More",
		"link": "site/more",
		"img": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAF3klEQVR4Xu2cvW8cRRTA3yS+RBY+44IKCtY0CAqwK5AQyrmkii0qJCTsBmS7gPwByI74AwLFnQWNLxISFcSpKO0IIUGVgwJEg5cCKiSM18hKbGd4s+uNz+vdnc+dmb3sFPZJOzsz+5s3771580GgSVoEiNbbzcvQANQUggZgA1CTgObrziUw2DgMgJ5cA0KD+FsozODfKfw/RUj8GyiFASqbPfy5h/8HST4SArl8L1weDzUZaL3uBGDQPZhBYO8hpE4KSfUrTuHuINDb4epEAtdisgYw2Ig68AihAcwTQqaq+EYKNMRO2YFLCHO5vVNFHdkyKwcYg6OwhhV1bHxQWgcFBEngZtUgKwOY6LbjW1jBvE1w2boQ5BaQsRtV6UrjAGNwj47WcJguugR3ASSlfbjUQok0a3SMAjzVc3eq0nG6HUIp3UP9uGByWBsDiJZ1kRC6qfuRNt6nlCyhxe6bqMsIwKC7v+nbkOXBQWnsh6uTS7x8vOdaAINbdAquHGzr+nK8Rlb1PLbUDyYWwhuEOelKSRlg3eE9dnfYLOfhxJwqRHWA3eh+XSXvooWGQbjanlURQSWAddR5PDiqOlEaYJ2sLQ/aRUmUt85SAJmfRyhsyzasTvkpgTkZP1EY4OkMA/VeNYEAXyAnznZrVnTGIg6wt98nQFg0ZeQTRnVuhyuTQlNRIYBM+gg93h15ckMfSMnYtIgUigHsRXdsR1XefnEMXnrmcvxJv/x9At/8dmy1/0SlkAvQtuFoXwH4/K1xeP3ZBF6afvjrBD749hCih/Y4ihgUPsBetG0zGPrxG1dh6ZVWLqXNn4/gk+8fWCPIpnrhSnuurMJSgLaljzX09+WJUkAvbBxYA8gq4klhOUDLlve5NoHv3n2qFNCbX/4Hf0YoG5YSTxeWA+zu/2Pb7/NOAnGhCl2a6aL+KgTIlh4xQHrfUkc/rsYnHZg2CgOws0VLpiUAo08x2vKhbYDMCn+BVvi1jBX+Ea3w+5at8BlA+AyjNR/lsSgD6DRcxfzAl4f8wK8t+4HDsNjifVG4Kxfgkzjz4I20oplJPsAaLRDxPtzUc7TGC2hMtrLlFQCM1lH/rZmqfBTKwWF8E4fxegNQsTflAPaiHRTNa4p1ab3GrHAaRMgW9CsGFWzOhTOG5C5K4IVtKvlD2CFA5r58dX08txPeuXsIzJ1xkXDucw/nxR3RIezMhfEWYIErkyuB0z2Lk81Ml/oKkDVzdwUn65nUAJTQB8IAg140QLKvSpRtLKuvEog68CfUgfGe7eHUGBHBrpc1IlvoSF8XLNtoNm8lkIKEG9N1NxPxGGA9ZiIjAtDdblNvAUoFExwupHsLsGChvTig6siV8RFgkQvDrKd3IX0vAVKlkL6bRSU/ASosKjHxnO7t4+Zr8rRRR49TmHcAKf1jd3UyKGq2VwvrrJG+AdRbWHewI9U7gJwdqyKbi6xGp30CWDT/5QYThjO42GBkU+eW1cXbWFTqxpyD2I2cBRdcweTpvrRd3CHMMj6JC+1Gt/jGEC1vdXMleaxeUekTHsJnUniElzrY9Qvtg6T/UtKaEdlgLgUwgdgctMl2qJAOPG9Q3IW6qpZGlYPY0gBHVR/K6D0pP7Co112u3JmWRBGHWWkuXNbQ+MD11QM2S3Gy/GkKIov14an1jvUD1/FQrjlEJnkIb14VnrQVLh7O9TuIqKrztK1wIcQa7WpVsbbGdWBegYmfSHEbrK/ONnOSybzMgWqerlVyY0qNS3xn1tG6b2eL2ZAF0loXnWHwwKXPjQNMC05OuB+zsyZOtoik7agKXOUAz0DG19+hRNrdMhxbWAIocdXeI1iZBGaHQHKPIF3ESys6QMjzokNEKh8uAGEQFO8NJP2qwVmTwFxjw64ABbqIEtLRdcRjR5jdWgkIbZSvAC10fxKjM4MHc9PNi3i/Kl5CixfRpnBjSOwCWhpfRJvck0ooXkzbGpg2ClISz5oh+0KT/zyBBqCmRDQAG4CaBDRfbyRQE+D/Cr7cb6T6wToAAAAASUVORK5CYII="
	   	}];
	 $scope.init = function() {
		Shared.onLoad("container", false);
	  };
	  $scope.init();
}]);

gciApp.module.controller("gciLoginController", ['Shared', '$scope', function(Shared, $scope) {
	 $scope.description = "Login to continue..";
	 $scope.init = function() {
	     	Shared.onLoad("container", false);
	 };
	 $scope.login =  function() {
		Shared.apiCall("login", {
			"gci_master_account_username": $scope.masterUsername,
			"gci_master_account_password": $scope.masterPassword
		}, function( data ){
			Shared.route("site/main");
		}, function(data) {
			Shared.warn(data.message,"error");
		});
	 };
	$scope.init();
}]);
gciApp.module.controller("gciLogoutController", ['Shared', '$scope', function(Shared, $scope) {
	$scope.showIfLoggedOut = {"display": "none"};
	$scope.showIfNotLoggedOut = {"display":"block"};
	 $scope.init = function() {
		Shared.onLoad("container", "logout", {}, function() {
			$scope.showIfLoggedOut['display'] = 'block';
			$scope.showIfNotLoggedOut['display'] = 'none';
		}, function(data) {
			$scope.showIfLoggedOut['display'] = 'none';
			$scope.showIfNotLoggedOut['display'] = 'block';
		});
	 };
	$scope.init();
}]);
			
			 


gciApp.module.controller("gciListmailController", ['Shared','$scope', function(Shared, $scope) {
	 $scope.showIfAutoResponderOff = {"display": "block"};
	 $scope.showIfAutoResponderOn = {"display": "none"};
	 $scope.showIfNotAuthenticated = {"display": "block"};
	 $scope.Shared = Shared;
	 $scope.composeButton = {
		"img": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAFBElEQVR4Xu2b30sUURTHz2zYrpK/tQwKNdCHNvwZaA+FYi9CQv0F9RRBPVTQc/ZcVA89RE/2FxQY+JIYRaSgqdX6oFFGgeav1ETXltzuWbwxM+7M7OyZXWfGcx9358y99zPfe893zu5VgBuJgEKK5mBggEQRMEAGSCRADGcFMkAiAWI4K5ABEgkQw1mBDJBIgBjOCmSARALEcFYgAyQSIIazArMJ8FLDQFFO7kY9sU9Xh8c2csd7xtqXUx1kSgq80tpXFQ/AbXHTS6ne2OPX9ShbcOfxYOe01TwsAQp4bVuB+DMFlCKrm/np+zjElwNbygUB8ZXZvEwBovIEvNG9Bk8C24bYaKZEU4CXT/X1KApc9JOy7M4lHoenT951Gm5dhgDvXX1fqUBsenJ0yW6fvrq+trEEPr5dLzZKLIYAH90cuV4ZLniwshiFkf6fsP475iswVpPJy8+B5o5DUFgagm+R1RvX7jc/TBZjCRCD/vz5Cx9ez8PstzWrfn3xfUXlAag7Uw779+9LzIcMUFL5PrUKE4OLEBNA/dhyBLDjraVwtKZAM720AN7qGjhXHS7o1d8Ml/SHN/OwsrjpK4aFpUGoO12eWLLq9vXTMnz/vNZ1t7f9ha0ljP5PmOcBlHP96YOQEwxo4iODC/A1krJhdzXs6nARhFvLNGOMbW7B+Ju5xLYlTHW7kR803AMlQLwrbqh1AmLZ4VxNJ7PTa6KTec8uaVyyJ89WQKluXgszG2KVzf1PnGSAklptUwlgWlc3TDAjL2dhUXTqpYbQmgU8mSjk2NG2Tb7XWjfHAGIn2DEuaVSlun0Re8XE0IInGB5vKYNjJ7RvpmjTcMkmE4KjAJEQSr9epHncH9XN7Z5R7e3U48Z9blzYNCN34ThA2Tk+xZqGEk2CwSU9IRLMj6nfrlLjkZp8YVHKNEsWE8XU2BLg6jFrGQOInWL6rz9zEApKgpoxuMUzGnm71aVNobq5lOxYRgFKamGxr1Qn2VdG+mdTGmQm5IoPt7mjYsd+jd4uYmO/zgpABGDkGTGrZbsogW4BXYO6qb2dnQeWNYAywaA90HtGzG7Dwu5k+jXQzNuh3Uqn/6wCtPKMmSxK6IsAZt7OtQpUD8xoD8qEZzTydk7swbuiQAnSzDM6UZQwKgI46QJ2FaAEiT4s3FLuqGfEIkBNU/EObxcZmnfUh7oCIILENwF8edd7RrtFiYSqRempokr7JoTeDhOV09Vz1wC08oxG76LqfdXoXdyut/NEEjEbJII42XF4R53RzDMaebvh/pmMVoNcp0B1gjHyjKhGuRRx6WMFKFndLl1v53kFqieARQm0Ieomf8jCz9Q/8MhrsHRmVQSwA8nsWtcqUO8ZkxUl9BOzUwTYUwDla2CyX8UkCCe9nR24nlCgekL6okS6RQA7kDy/hPUTkJ4RP8+Et7MD13MKtDO5bFzLAImUGSADJBIghrMCGSCRADGcFcgAiQSI4azA3QCIp5KCedFfxL59Eb65HrL/J3OcOR9zAEj7mAMCTBy0UWBMnBUp9IWUbE5CwFsJxKEh7YM22xDbBMTnew3iNrzzpKNe8oFtK7F7r5xawmUrlNftyGFDteoxsYRC0QabK8FTl0ejoTHHj7t6ikCWB2t53DXL4/FcdwyQ+MgYIAMkEiCGswIZIJEAMZwVyACJBIjhrEAGSCRADGcFMkAiAWI4K5ABEgkQw1mBRID/AC/562/MOzCbAAAAAElFTkSuQmCC",
		"title": "Compose",
		"link": "site/sendmail"
	};
	 $scope.userItems = [
		{
		"title": "Authenticate User",
		"link": "site/authtoken",
		"img": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAJfklEQVR4Xu2cW2wUVRiA/+m2BYHSLhSJjcj2ATAq4dYHYoxsYyIRQcE04eKDrZGLT4r18gaFN6MVfCug0j7IJWmUmxo0pq2JBpMCJSgReGCRpAYp7G5bLgW64//vpd2dOWfmzJwzs93YP9lAd2fOnPPN/5//P/+5aDAuUgQ0qbvHb4ZxgJJKMA5wHKAkAcnb866B0ejgwmFILNBAD+mgL8T2VOAnBKDhJ1v0CP5Fn5gGWo8OWiQAReeCwSk9kgykbvcdYDSqVwzDwKsAidVY8zCCImASosewjCP46QxA2dFgUMO//RPfAN6IDoQ1SLyBTav3uHmtOhS1zQiWdXr8nGTxngPsi/YjtEST2SS9bh6ZfFFTZXBqm5dP8gxgSuOG9/sPzohLj+gQaPBKI5UDjEbvhoZhaBeCoz5OSC7eTEBX5GHy2r/6sIe8r8PAEMAl/J5k7vQiKJsAUFaqwZOVgeR3y0LFMA+/Fxf9SADKG1T3kUoBorkitARpnaVj6L8PcKZ3GDoiD6DzykMEJo4h+8qyUvRC1cVQGyqBxVUBmIp/Wws5nKIGNGt0OmpEGcAb0fguLOxdq2qRpu3pHoLOyLCa2htKCYcCsLlmgq1m6gC7ZwTLt6qohDRACkseQrwDYzOK4ZjSO6DDntNDcPxiyky9llXzimHzkglQVcZvHsacPcVQXitr0lIA7eCRqTb/ds83cMYXQyAbn53INW0VEF0DtIPXgWba1HHXdf+mSkupn2xePglqqtgORxaiK4B28A6efwifoOaNJfkANXH9/GJmlWQgugJ4Ixo7y+vztnfmz2TtXhiZ9I7wRC7EGcGKRXZlGH93DLAvGt+NhbxjLIj6u03H7ozEbk4r4tf1FFPufWUSr1/8vDJYbhlJSAFMxXn6t6zGrmsf+/Ay9SaIh+omcd6ZtsZJnCisgekRxllWkKzSbOvmT4Kls0rhqZkl8PSjJclG/vnvA7hw/QGcvHwPfsKPCuGbsx7DEUu1aHgjDLAvGkPNMw/PDqDD+FSBw3hxzkTY9kI5zCpPDdV4ci0+DDt/jsOPCkC+j45lA9Ox6EcqgxVrRF6UEMB0KqrDWCCFKo0n74o8x/Ka7QjuzZrJjsr5svt2EqSs7F3FDnEwJVYrkoAQAojad8WYVSGnserrQek4jwXvAprsqb/vJ02XhEx56RNo1mmTzkD7CiHukIRIceLx16cwnIoeQS2stntBtgDRcdSj48AEQa5s67gHJy7JDc3IbPe9Nm2k4P4hPQmk/fwdZr2pfyTgUyeMVnvjN7ekzXkdmvGHaM5m0Sjx0GoFUQCgWftobLvywG27l2P7+69vz4THp6b6PIK39kAfkPZZCWnh4Q2VIxCpT3yu5brts+wuOLFhsmnsTAG2XWxoCZDX96nQPtKm5hWjWa/G72NczTM23nivCi1cObcYdtaatdCuL7QEiEEzqS/NY4wIpaTWY8wnK80vV0DdM6lYjLTupf03HBWZrb3tf9yBxu/k55JYWoiVasPgup5XOS7A1OxZf9R441b0ul0K8nk/NMwYcQpunEG28yFns8LhC2ABWYb5xF3LHzH9hKnaIC8u5AJkOQ/yvOH9g440hXfx1Y+qRn5yYr6ZmyjsIYgZmf1xr5J6dTawPDLfmVgAjFPaG+dvR0VV3EclZgNce7AvGbY4EQprDq+vVA6wGTWwFjXRIFwztgAYQ/PNndtQ4TwyFRurANnORI9hTBhkvWAmwNRyi2Ec9+bKMjRftxNAxrLGKkAKrLvQjI0SgMAi1jISJkBW/6fK+451DaT6HcRMjXnKlN0PMgHiDFsT/rA9+y20dN+Hvaed9VNWfdpY1UCq86YlpbClJneOFGfyduBMXpOxTRwNNDsQ1QCdOAy/r2UBxDocxXjQtFiAo4GxTkzZL8uu+FuYbT7zT2qlgFMhj6njK/Rbfr/mzmJY8SAO67pwWBcW1EDz+FcGYLa5+gnRbWy4+LEi+ALT/rnCzs7wTNikLzIp+0IDyEv5owmbeAkDXLzH/Qik0ACS5p3ZbA5l8gbQT7NV9SzlANe234bLN915AidOZADzgrycIOUCy7KSqXaw3DqROdM1OFxnnmJwoIGxCA7jZqvywk5M+BR6TkqssoQSqTRjJyqKnchVHM6FhLwwrjxQGsYUGkAFYYzaQLrQACoIpL0fyomaYT6uUzCUM8/EdfcmYNNxd6n8QnMi0skEXjrreUxnDboYHRWSCU9BH/WLbDqLzKbvViwGmjaaM8fv3CZUCwkgM6Gq6/HKaRXMhfO+pPQLCaDClL65H1Q5qZQP5yDyTGWTSiqnNZ04EZFGil7jdCSidFoz2Q8qmlh3YsKicESuczoSUTqxThVUtbSjEAB6srQjpYXmcbHTxUWFAJCzuOgcZqG5G4iIj8DqLO+Wt4mYoR/XeLq8jaeF5JFX4gJLN4G1H1BEn0GB8wn2Aktm9kUoG2O8iNcXygzvRBvo9XW+LPFNe2TTWhn6XtUic69BscrnLzJnT2GyyrDtAzM3JeNCPR4xDu/od7dDvHxAyzyT53Vx/jUe0MpDyrc5pLSQv9FGJuXvN0heyj5VD4822mQaabXVa+Mx9/MmfkEkePtemZyfrV6ZRmLKvwdXLixgNXosmzPXbLEhuPLANuaT6gOzb05vd6V5EybEsehYLBxGEh7uXg+L9nvZLISdiJG+HUQKcd47eSfvcSLFeZ9Zb7h2DS/ZY8r0PXYQKdhu6b4Hh3A/XT6ERhhbaiy3/EvBkwZIBdhBpGto7NyCp3XI7mwSfQnU123B0ztsDp2QhqcEoJ13zm40gaSjAFRsk2DBpHwebe23Ape+z/HGat7LkzJhY6HJOFFPtLKC7exrybRP08E7V/DgHTyxyO14mvq3MJ5gVFtdAktEDt7BIBm0ononG6rttF4pwIxJ4wadVvx/zhYJq4rQ+usOPMGI5OJNPPoJ18fQ8U+ZtTgUu9GxT7QuZt701BaEWjyxyNnRT3AUN8zUu/G0VnVXDjDzsPThYwgyd42N3RtV/7t+FQ8fqxfZ++vm2Z4BHO0bKZ+YPP4uZ7GSm8o6uYdiOw2KdtttV3VSJutazwHmamQCYeZuXpRtAOP+Ntxh2eqVxhmf5xvAzIPTR4DS6R9hzHystnM4toCTjmHkCNAjqvs4u+f7DtBYofQhtAvR5EJYGfq3Av8NmU2e+jKI4PAxhv/SWJwOoe353x1Ca/dGC+33vGtgoQHLex9Y6MDGASp+g+MmLAn0P0EJsY0jbl4nAAAAAElFTkSuQmCC"
	 	},
		{
		"title": "Edit Autoresponder",
		"link": "site/setupmail",
		"img": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAIK0lEQVR4Xu2cXXYTNxTHJbdAe7BTWEEnK8CsoMkKSlbQ8EjyAKygyQriPCQ8YlaAWQHpCggryLACQpye4pBa/UvjcWJ7dHX14Y9yxi8cYKSRfrq6X7oaKepfFAEZ1bpuLGqAkUJQA6wBRhKIbF5LYA0wkkBk81oCvzeA2dFlG77BIyFVNp6bEhtCqVw0ZD7+t3/VqZCNT/lu8zSSQVTzpUtgAUz9gVm0MZiNkNkoIU4A/UQM5btFA10KwOxAPRA/XT6HVG1LIW8kLYTeVBslIKlSdsXX5mH+Up4n6JLsYqEAx+CG6oWU8sE8J6eUOseW78wb5MIAYqs+F2K4N29w04syArmVP2udzGPB5g7QSN29y7eh+k1PGjruoxTaqMhfQiGgj44YNPdTb+u5AsyOL55g9q+5UqeUeAdAp6IBg/C1eWqbbPbqH8C8zoSSbejRDSkVjI8bLvo/FVfNzZQQ5wYQW3YbE3vtkhgtXQDREVf3ezETM4sl5AtM6DfqnakhzgUgC55Sn5Ro7MHt6Log+/x/9qoPn1F0MLFHtnYpISYHyIGHCRzmu60XPmB8n82OLyGN6sAOUXXz3bWnvv1OP58UoHaKsW0/2AelvmDLbuc7a73YgXPaa2mUSuFd1fpRKfk0dgckA2is7d3+md1gAJ5qbCw6UigWdQgXZhaicXGuWusxujcdwOP+e7urshx4pZRqA4OI522V1CJyeYMdsc2R6KpnkgCkBmj8OCUfL1rypiebHfU7Ugo487M/JX9cz5/9nIdAjAZYOMr9D7aYVgn5Mt9pdkIGdyNB/RvXZND8GLLl9Djl3T4yOPLX6bHESGE8wKM+wjPxZ+XKwjGGtYV/5vczjvLwG/qU0F+iXb314Gwr2RONH95xpYfyENSg+TBkYaIAmlW9d/nZ6ip4bg0DTl0fYFBe0GEMuqJxZ58Dcv3oQmdrZqUw0CLHAaSlbx/St8eVPY7/6OpLSbGHpME+9Rx0YeWO0TnFfKe16XpHUj8wO7r4XO22wOoOWhl3S6AfHS8HW8Lbk3LpMy3lUl2fVaqFgG0cLIGkPvGINFLCMxZfiL8gSRukFB73T6tCPcDf8nXyIwD2e1Dwv8e4BfxtqyMYac4+EFlkVTqsHAcLoMWlQYjppXbMeHz3vH6eMh6cCZg+Ckv7gUp1FektqRMOEwdHpGPMkH5be+7YbzMLA0ikqrjxZUZGLsb5tsapRYwr3ltcJ6cU2WJ2HdohwfDQR6gCAcZtXwqA0WOOrWSzpJy2JZz14z4EbvZ3ttPyYuL1cPk6m/XVyVEo8ErH9/ZQKenjbCMSoBSbnPOP9eMLnNhVJBg8fVdvgKQbwNE/xPYzEsQAQMe1PIBYxJOq7DXn/VE6kFbg7vwa2ncRN+uD9NkfstRnu2uZSwfZJs9dAGPElgaQij5Y0mNzvrUPx0s8UAC5OmyZAK3+n2vwqdJe2MJwf6qTDK4xjPX40iTQ8mLIz5eznTWy2oBS/pz2Lgvq08fyJNC2crwQqlJxG93FaO8C6NPH0gDa/CfO4G1tffw30gvwWASbGvDNnnu7MXaA9NmCM3fINSBUFOJxvrE0R9oK0BU9JPD/ihg6LoxzqQGuESr7SSeBiwJIHJi7QsCxBbYsAjeSinKkgyWQOFr0coDJLLjbkS+c6OqqhZCKiXQS6NA/tAvDC+HM5CMd+VEflb4sN5M0Hwl0WEBX8pQbg1JONKcPMpfpmUjQIAMk0JLFcAF0GRHGqZgzDcY407AtZIj+CwJodUBR3I3zhHVbIsA5eYYL4krCciwowsmzqiKAkO2bFKDuzDUBWw6ucKTpQh9XuVphiOgSDbv+9DtFjNKBdDaYroFBW2siYhTO9ZCQ3ZqWYlcSonyePAYgSu+47k/V7vLWgfRxJu1GuAzJCCIuzKgOriic488H0NJPuGfGRopFY3PmEApnOLghcFB5gKUrZa9abe4Z9jTEEIDWIkrXobZ+ua20wqY7ff+9gChQMzO6FjbUC1Cd+vLxP23j8AZoINjOExinWhwpJKFpicG1LmtW24N4qOGI0oG6MaXLWL4Yldan6ZkqV1T057ZSNS6/GL2XAKD9CgNnG48u3/SqDnWsAIqq/ielfqNKd2mIusoBtwMiaxbLdwRtYWdqiunRu8K7G+sqDnFBZm9a0fsuhF5cIe+ggiusGjWJFS47oU7XOFI47sdUuP4NK6lMTSCuJrTLOhj8rScG97suC1lcmR3i2oTMdPvyvFcneZHq1ue/vdiLPEmNiNGDrtCMcULH1Ver/FzQFr6RwurDaeMe6Hu7g9Zjl/SsMhzO2OIAuqQQ/lhVZMEZ2P/lmSiAZitbjzkLBLp+OcWVqlUFGg/QFJr3c+q6qfmmwaC59T1u52iAHIMy1okSsfKcbo4vS0KTADQQiVT77ckVVxLkm5Qgi+sR39q+9c0poCcDWOhD+orpJEjcHhf4uobHRZnb7Y3vpy9XS4V7cGIjpLp05QAWksi7qT4BU7s8uohcAuoQaSyJj+pM/CQKy1FcroSuvdHX/JERmv3qh29VwUoCLCRR344UXc53DFJMouyDWx6X8p1Jt/DEFjP3hy+7tqsQKScxBhh4Ny9mLHMDWA5q9A2DPa/MS+CMQk/WAl9nms0d4ATIocAXi3ifKPGalEmyNjqpUlQ+714YwIntbT5Rom9k4jor8XUNaiLF51LgoMOSL/My91IAToMZpaOKD+mUP/P5O1hfA2n009ZZNfJlApse+0oA9Nkyq/ZsDTByRWqANcBIApHNawmsAUYSiGxeS2ANMJJAZPP/AFUUCJyDIp52AAAAAElFTkSuQmCC"
	 },
	 {
		"title": "Edit Signature",
		"link": "site/setupsignature",
		"img": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAFr0lEQVR4Xu2cXUxcRRTHzyzQZflYtygFuiylCXbXJihEmtQHCk3qEzEuJWks1XRbjImlxuJH/XiQ8qK+KNbEJmqhS+rGxMZCKBsfalIoBqlWS9TYWBsFltLQYiG0ZVsqO96B7AoLt3fuzty7H8w8wjlnzvzu/8yce+/uIhCDiQBi8hbOIAAyikAAFAAZCTC6CwUKgIwEGN2FArUAWOn0lmCAlwFwIWP8BHFHg5LSDnd3VA2EL2hZBVY4uyYAIUuCrJ7PMjAM9nRUracDWO2VBChGOIGe9qolgltegQLgsuoRABmLSgAUABkJMLoLBUYDYKEtE9JMSYxTx6f7tH8WBn03Q8lHpMDGV0vxxg2WFXnH8vulSdz0wYXQ2gVAlYUgAKoEFm4uAAqAjAQY3YUCYxVgz/dX4fqNu4zpqXev2JwD2Q+aZB2v/+OHnv4x9YElj+wsI1Q8kbfIVzMFfu0dhJGrtyNKlMWppqoQ8vPSZUOQnEhukQwSl8RfOARAFSR1BUhKeDwKJbyFooTPhpXwzL07sColVRHlQ3qWsGI2MWLwmecLGB4dhbfq90N6WprqrDQrYdWZRMGBwOv98Ye5mddZ8yOCuCIBolP9MH6kHZqt0zCUMX8bW7DWCm/vf0m1ClcUQAIOf/4NoNEbc9BuJ2F4txgB3pAfETwSQzOApN+6OxPQvTCzs1LBaFz8aA2d/xPgw5MAl0aW5ONPQeDzvAMmk3zvGHQyrjIs6TE1AxgTfeDU9Bw41HVO9kL+VlQE327bRnWhdW1jog7wDx/A60dD5SpHqLW2FqbMZgEwSIDcKdjO/zpfsjf99wXTX/Y49JdtooJHjHRVIHVWnA3JQQFNHuWoGSbAnYcAzOp7v4XBNdsDlVfA34IanjQ1fmU7QO1W5iQSBqAaePCwFfCXbzLD07SN4ZIdZRBV8Ij6PAcB7DbK6Pc3i3sFqoUHjbsAP7WZC7y4V6BaePiZSoDXarjB4wZw944iIC/X9RyWnp/B+mk79ZST5aVw5UXp4OA8yEv1tq8uh6JG9F6Yc06K4SrHh2Df0E+KdkGD7qwCOLK+jNqexTDmAaqF580ugraCR1mYqPKlBlju9LoMCI6pis5orBbeiTwHnFi7kXFWevcAhj29HVXucI+Y+MzLNdteF0aI/oJhaMvxtbjol6+dZdQBxjM8clmiCtDf9MjR6dOOun+Hs6gkggE35A63fkRlrJNR1ADOdmTsM6y+9wm+lQIT7z0JShBxAA7njrQc0IkL9TRRAXirftP76Tt+eSOYpSLEGNrzon6IBPc88wt9kFr+VygfWYgxDE/3PTD8wFCCGKtlu1CFupWw3GkrD3F1U85w6yHqzShKhroAVGpVwiHeOWe7aDp4Wb8umQG+5gCV4AVzD0LEE8ZOg3Pq6WZPewn5X8Ou6iXfkGRYL3dXTQHSwgtBrOtrMTVefJ7AQwF8hvwdG9DWWIaoGUC18BDGe9b4Wt0heAjmv26LYRIno9KGndWD3OXDIaAmALnBm+MHbQ3PbndxWKsmIbgDXEnwuPeBY7Y6p3R3Tf0oWbZs40B5QTlzU+CExWWZMRv+lp5PUP1UQCLA46rAsYK9UtOLGmk2mi57+nd1pz8uX3JgxJHyuCtQKl+3VL67lQAeLzVD/zoTPGDO7Cy227dIPiHFxvqBsdzauJUwjQI9JZn+vsI0E/m8crHDDsnJ/3/WLx7hcS3ha1ZXCU5KuiCnQLLn1dfkDqSlmnqLHY6MRIDHFSAJJtfCLDwwYDZwFhlQ6EVzvCqP+x4YDEiUGEBJLkC4BDAaMOBZ95or7oFEODA03QOVDo/m4ye7EYKKoF28K08zBcqBbD7WboFkTCA+lijwuO+Biiqcgxg40PBcjdQzJsbg1sYkBg71qxAA1TNb5CEACoCMBBjdhQIFQEYCjO5CgQIgIwFGd6FAAZCRAKP7fx9WY345DeaQAAAAAElFTkSuQmCC"
	 },
	 {
		"title": "Threads & Notes",
		"link": "site/listthreads",
		"img": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAADlUlEQVR4Xu3bTWgTQRgG4G822lAaS1SqJmosKIIo2F7VQwLiSQ+CNw9tKehBhAqC10IRBdvQS8WD0NSrglB68ZTUVIReJIg/0UCTi6CICtXWpibjbuPWNE13d3Y2yfwFQiBkdmaf+ebdCewiUC8qAUTVWjUGBUhZBApQAVIKUDZXFagAKQUom6sKVIDkAt+HhoLLPl8fxrhH07Sp0OhoivwolRbSVaCBt+TzJfVz7zHRdMiBA/F4wg2iVID18GgRpQG0wqNBlALwafJVEOGl5P7s21/h3PvTVkuVdDkLD2jimZmnI77wElFowFo8s/K8RBQWcCs8EkQNoZjdFkdIQDwJwenI7AxG2yzzzq4SdZyp8NhYv1VmCgdo4MEKJL/5T7SnDz4IgYY6rQCsEBHGN8Lx+Lg0gCaeecHQEbNuEfWrcaajXI7uHB//IQVgLZ550m4QneIJ81duKzw3iKGP7wJOKs88NvcZaIdHgogBZ6LTT2yXbfWS5hrQKZ4TRAMPUEf0YqzXMvNq85BbQFI8K0S3eNxmoFu8eog0eFwC0uJVI85FJr6Wtc7zpMuW2wz0Cu8fQAb8EEUDQJR53GYgi3jcLGFW8bgAZBmPeUDW8ZgG5AGPWUBe8JgE5AmPOUDe8JgC5BGPGUBe8ZgA5Bmv5YC847UUUAS8lgGKgtcSQJHwmg4oGl5TAUXEaxqgqHhNARQZr+GAouM1FFAGvIYByoLXEECZ8DwHlA3PU0AZ8TwDlBXPE0CZ8agBZcejAlR4lduMXN1gqfAqeK4AFd5/PGJAhbcRjxzwWds9KBSvQBksn/7Z3M2mbzy5udFBPw3/CVEG4pQ/D2X8G3LFEAWiMHhEFYiT0A2af2FtSks46xJRKDwywOfb+wFrk+trghxRODwywNm2hL7r6dsQKs4RhcQjAzTyD8GhTalsjygsnmPAieHjZ84e+5w+um+x/lVta0Sh8RwDjtzsfZj6EBq8f3keCBCFx3MMeP3qqdx8vutwwL8KDhEXvHiIpeGbOA86cLQPvHDp3J8vi+0+oz9LRAwFWMGP4VPxNu0TQB6cW1MOYQto5N+jl0fS1aNZR9y7WACEU2vv0moKxSDflFEz1IktoJF/M68jg8aY9+xYLnXv/pnXP1O7AsXEteE3cwydS0uGYgt499bJO6slrUuB1Z8fW8CWTCtHnSpAyslSgAqQUoCyuapABUgpQNlcVaACpBSgbP4XkhpVbyFNkecAAAAASUVORK5CYII="
	},
	];
	 $scope.init = function() {
		Shared.onLoad("container", "listmail", {}, function(data) {
			$scope.user = data.data.user;
			$scope.autoresponder = data.data.autoresponder;
			$scope.checkAutoresponderStatus();
			$scope.checkAuthenticated();
		 }, function(data) {
		   	Shared.warn(data.message);
		 }); 	
	  };
	 $scope.editAutoresponder = function() {
		Shared.route("site/setupmail");
	 };
	 $scope.editSignature = function() {
	  	Shared.route("site/setupsignature");
	 };
	 $scope.authenticateUser = function() {
		Shared.route("site/authtoken");
	 };
	 $scope.checkAuthenticated = function() {
		try {
			var parsed = JSON.parse( $scope.user.gci_user_gmail_auth );
			$scope.showIfNotAuthenticated['display']="none";
		} catch ( e ) {
			$scope.showIfNotAuthenticated['display']= "block";
		}
	 };
	 $scope.checkAutoresponderStatus = function() {
			if ( $scope.autoresponder.gci_autoresponder_active ) {
				$scope.showIfAutoResponderOn['display']="block";
				$scope.showIfAutoResponderOff['display']="none";
			} else {
				$scope.showIfAutoResponderOn['display']= "none";
				$scope.showIfAutoResponderOff['display']= "block";
			}
	 };


	 $scope.toggleAutoresponder = function() {
			if ($scope.autoresponder.gci_autoresponder_active ) {
				$scope.autoresponder.gci_autoresponder_active = 0;
			} else {
				$scope.autoresponder.gci_autoresponder_active = 1;
			}
			Shared.apiCall("setupmail", $scope.autoresponder, function( data ) {
				Shared.warn("Updated auto responder status", "success", function() {
					$scope.checkAutoresponderStatus();
				});
			}, function() {
				Shared.warn("Unable to update auto responder", "error");
			});
	 };

	$scope.init();
}]);
gciApp.module.controller("gciSetupMailController", ['Shared', '$scope', function(Shared, $scope) {
	 $scope.autoresponder={};
	 $scope.description = "Autoresponder will be used as the default response for all responses for this user";
	 $scope.subdescription = "A  canned reply can be used as a template";
	 $scope.showIfCannedReplies = {"display": "none"};
	 $scope.showIfNoCannedReplies = {"display": "block"};
	 $scope.currentCannedReply = {};
	 $scope.replies = [];
	
	 $scope.init = function() {
		Shared.onLoad("container", "listmail", {}, function(data) {
			$scope.autoresponder = data.data.autoresponder;
			Shared.apiCall("listcannedreplies",{}, function(data) {
				$scope.replies=data.data;	
				$scope.checkCannedReply();
			}, function(data) {
				Shared.warn(data.message);
			});	
		}, function(data) {
			Shared.warn(data.message);
		 });
	 };
	$scope.update = function() {
		Shared.apiCall("setupmail", $scope.autoresponder, function( data ) {
			Shared.warn(data.message, "success", function() {
				Shared.route("site/listmail");
				});
			
		}, function(data) {
			Shared.warn(data.message);
		});
	 };
	$scope.checkCannedReply = function() {
		if ($scope.replies.length>0) {
			$scope.showIfCannedReplies['display']="block";
			$scope.showIfNoCannedReplies['display']="none";
			$scope.currentCannedReply = $scope.replies[0];
		} else {
			$scope.showIfCannedReplies['display']="none";
			$scope.showIfNoCannedReplies['display']="block";
		}
	 };
	$scope.useReply = function(cannedReply) {
		$scope.autoresponder.gci_autoresponder_subject=cannedReply.gci_cannedreply_subject;
		$scope.autoresponder.gci_autoresponder_message=cannedReply.gci_cannedreply_message;
	};
	 $scope.init();
}]);

gciApp.module.controller("gciSetupSignatureController", ['Shared', '$scope', function(Shared, $scope) {
	$scope.description = "This will be appended to all responses for this user";
	$scope.init = function() {
		Shared.onLoad("container", "listmail", {}, function(data) {
			$scope.user = data.data.user;
		}, function(data) {
			Shared.warn(data.message);
		});
	 };
	$scope.update = function() {
		Shared.apiCall("setupsignature", $scope.user, function( data ) {
			Shared.warn(data.message, "success", function() {
				Shared.route("site/listmail");
			});
		}, function(data) {
			Shared.warn(data.message, "error");
		});
	};
	$scope.init();
}]);
gciApp.module.controller("gciSetupNotesController", ['Shared', '$scope', function(Shared, $scope) {
	 $scope.description = "";
	 $scope.init = function() {
		Shared.onLoad("container", "getthread", {
			"id": Shared.getId()
		}, function( data ) {
			$scope.thread=data.data;
			$scope.description = "These are the notes for thread "  + $scope.thread.gci_thread_title;
		}, function( data ) {
			Shared.warn("Unable to get thread. Is this a correct thread id?", "error");
		});
	  };
	 $scope.update = function() {
		Shared.apiCall("setupnote", $scope.thread, function() {
			Shared.warn("Notes were updated", "success", function() {
				Shared.route("site/listthreads");
			});
		}, function() {
			Shared.warn("Unable to update notes");
		});
	 };
 	$scope.init();
}]);

		


gciApp.module.controller("gciCannedRepliesController", ['Shared', '$scope', function(Shared, $scope) {
	 $scope.description = "Canned Replies can be used as templates for auto replies";
	 $scope.cannedreply = {
		"gci_cannedreply_subject": "",
		"gci_cannedreply_message": "",
		"id": ""
	  };
	 $scope.init = function() {
		Shared.onLoad("container", false);
		};
	 $scope.save = function() {
		Shared.apiCall("setupcannedreply",$scope.cannedreply,function( data ) {
			Shared.warn(data.message, "success", function() {
				Shared.route("site/main");
			});
		 }, function( data) {
			Shared.warn(data.message);
		});
	 };
	$scope.init();
}]);

gciApp.module.controller("gciNotesController", ['Shared', '$scope', function(Shared, $scope) {
	 $scope.description = "Notes for message threads";
	 $scope.showIfThreads = {"display": "none"};
	 $scope.showIfNoThreads = {"display": "block"};
	 $scope.showIfPages = {"display": "block"};
	 $scope.currentPage = 0;
	 $scope.init = function() {
	 	$scope.listThreads(0 , function() {
			 $("#container").show();
		});
	 };
	 $scope.checkThreads = function() {
		if ( $scope.threads.length>0){
			$scope.showIfThreads['display']="block";
			$scope.showIfNoThreads['display']="none";
		} else {
			$scope.showIfThreads['display']="none";
			$scope.showIfNoThreads['display']="block";
		}
	  };
	 $scope.listThreads = function(page, callback) {
		callback=callback||function(){};
		Shared.apiCall("listnotethreads", {
			"page": page
			},function( data ) {
			$scope.threads = data.data.results;
			$scope.pages = Shared.checkPages( $scope, data.data );
			$scope.checkThreads();
			callback();
		}, function(data) {
			Shared.warn(data.message,"error");
		});
	  };
	 $scope.goToPage = function(pageNumber) {
		$scope.currentPage = pageNumber;
		$scope.listThreads( pageNumber );
	 };
	 $scope.goToMessages = function(thread) {
		Shared.route("site/listmessages&uid="+thread.gci_thread_uid);
	 };
	 $scope.goToNotes = function(thread) {
		Shared.route("site/setupnotes&id="+thread.id);
	 };
	 $scope.init();
}]);
gciApp.module.controller("gciNotesMessageController", ['Shared', '$scope', '$sce', function(Shared, $scope, $sce) {
	$scope.description = "Messages for thread";
	$scope.showIfPages = {"display":"none"};
	$scope.init = function() {
		$scope.listMessages();
	};
	$scope.listMessages = function(page) {
		page=page||0;
	
	 	 var threadUid = Shared.getQueryParam("uid");		
		if (  threadUid ) {
			Shared.apiCall("listnotethreadmessages", {
				"threadUid": threadUid,
				"page": page
			 }, function( data ){
				$scope.messages = data.data.results;  
				$scope.thread = data.data.thread;
				$scope.pages = Shared.checkPages( $scope, data.data );
				$("#container").show();
				//$scope.checkMessages();
			  }, function(data) {
				Shared.warn( data.message, "error");
			 }  );
		} else {
			Shared.warn("Unable to get thread.");
		}
			
	  };
	$scope.getMessage = function(message) {
		return $sce.trustAsHtml(message);
	 };
	$scope.replyTo = function(message) {
		var url ="site/sendmail&replyTo="+message.gci_message_uid;
		Shared.route( url );
	 };
	$scope.goToPage =  function(pageNumber) {
		$scope.listMessage(pageNumber);
	 };
	$scope.init();
}]);

gciApp.module.controller("gciListMessagesOuter", ['$scope','Shared', function($scope, Shared) {
   $scope.init = function() {
	var threadUid = Shared.getQueryParam("uid");
	var url = gciApp.defaults.baseUrl+"/?r=site/listmessages&uid="+threadUid;
	$scope.url = url;
   };
  $scope.init();
}]);

gciApp.module.controller("gciSendMailController", ['Shared', '$scope', function(Shared, $scope) {
   $scope.mail = {
	 "to": "",
	 "cc": "",
	 "bcc": "",
	 "subject": "",
	 "message": ""
   };
   $scope.showIfReply = {};
   $scope.init = function() {
	 $scope.mail.subject =  Shared.getQueryParam("subject") || "";
	 $scope.mail.to = Shared.getQueryParam("to") || "";
	 $scope.mail.cc = Shared.getQueryParam("cc") || "";
	 $scope.mail.bcc = Shared.getQueryParam("bcc") || "";
	 var replyTo = Shared.getQueryParam("replyTo");
	  if ( replyTo ) {
		Shared.onLoad("container", "getmessage", {
			"messageUid": replyTo
		}, function(data) {
			$scope.mail.message = data.data.gmail.message;
			$scope.mail.to = data.data.gmail.email;
			Shared.apiCall("listcannedreplies", {}, function(data) {
				$scope.replies = data.data;
			}, function(data) {
				Shared.warn(data.message,"error");
			});
		}, function(data) {
			Shared.warn(data.message,"error");
		});
	} else {
	 Shared.apiCall("listcannedreplies", {}, function(data) {
			$scope.replies = data.data;
	 		Shared.onLoad("container", false);
	 }, function( data ) {
			Shared.warn(data.message, "error");
	 });
	}
   };
   $scope.send = function() {
	 Shared.apiCall("sendmail", $scope.mail, function(data) {
		Shared.warn(data.message,"success");
	 }, function() {
		Shared.warn(data.message,"error");
	 });
   };
   $scope.useReply = function(cannedReply)  {
		$scope.mail.subject=cannedReply.gci_cannedreply_subject;
		$scope.mail.message=cannedReply.gci_cannedreply_message;
	};
   $scope.init();
}]);




			


		
	

		
		

		

		
		
	 

	 



