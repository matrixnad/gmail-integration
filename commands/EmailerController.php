<?php


namespace app\commands;
 
use yii\console\Controller;

require_once(__DIR__."/../vendor/autoload.php");

function base64url_encode($data) { 
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 
function base64url_decode($data) { 
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
}
final class EmailerController extends Controller {
	 public function actionBackground() {
		$gmailComponents = new \app\components\GmailComponents;
		$appComponents = new \app\components\ApplicationComponents;
		$config = $appComponents->getConfig();
		$timeElapse = (3600*1);
		// sender-time-subject
		$cache = [];
		$singleOptions = ['format' => 'full'];
		while ( true ) {
			$allUsers = new \app\models\GciUsers;
			foreach ( $allUsers->find()->where([])->all() as  $user ) {
				if ( $user->gci_user_gmail_auth ) {
					$this->logData("Using Authentication", $user->gci_user_gmail_auth);
					$googleClient = $gmailComponents->getAuthorizedClient( $user );
					$gmailClient = new \Google_Service_Gmail($googleClient);
					$pageToken = NULL;
					do {
						$opts = ['pageToken' => $pageToken];
						$messagesObject = $gmailClient->users_messages->listUsersMessages("me", $opts);
						if ( isset($messagesObject->messages) ) {
							$messagesArray = $messagesObject->getMessages();
							foreach($messagesArray as $messageListItem) {
								$message = $gmailClient->users_messages->get("me", $messageListItem->getId(), $singleOptions)
					;
								$messageInternal = $gmailComponents->getMessage($user, $message);
								//$decoded=base64url_decode($message->getRaw());
								//$this->logData("Raw Message",$decoded);
								//$lines = $this->getMimeLines($decoded);
								//$subject=$this->getSubject($message,$lines);
								//$sender=$this->getSender($user,$message,$lines);
								//$time = $this->getTime($message,$lines);
								$decoded ='';
								//$this->logData("Read message",json_encode($messageInternal));
								$time=new \DateTime;
								$time->setTimestamp(strtotime($messageInternal['date']));
								$subject=$messageInternal['subject'];
								$sender=$messageInternal['email'];
								
								$this->createThreadAndMessage($user,$message,$sender,$time,$subject,$decoded);
								if ($sender && $time) {
									$cache[$sender]=isset($cache[$sender])?$cache[$sender]:[];
									if ( $this->needsAutoreply( $message, $sender, $time, $cache,$config, $decoded ) ) {
										$cache = $this->sendAutoreply( $user, $message, $sender, $time, $gmailClient, $googleClient, $decoded, $cache );
									} 
								 }
							 }
							$pageToken = $messagesObject->getNextPageToken();
						 } else {
							$pageToken  = null;
						 }
					 } while ($pageToken ) ;
				}
				sleep($config->EMAILER->PER_USER_TIMEOUT);
			}
			sleep($config->EMAILER_PER_CYCLE_TIMEOUT);
		}
	 }

	public function createThreadAndMessage($user,$message,$sender,$time,$subject,$decoded) {
		$thread = $this->createThreadIfNeeded($user,$message,$sender,$time,$subject,$decoded);
		$message = $this->createMessage($user,$thread,$message,$sender,$time,$subject,$decoded);
	}
	public function createMessage($user,$thread,$message,$sender,$time,$subject,$decoded) {
		$gciThreadMessage = new \app\models\GciThreadsMessages;
		$gciThreadMessages = $gciThreadMessage->find()->where([
			'gci_message_uid' => $message->getId()
			])->all();
		if ( !$gciThreadMessages ) {
	 	$gciThreadMessage->gci_thread_uid=$message->getThreadId();
	    	$gciThreadMessage->gci_message_uid=$message->getId();
		if ( $gciThreadMessage->save() ) {
			$thread->gci_thread_date_updated=$time->format("Y-m-d H:i:s");
			if ( $thread->update() != false ) {
				$this->logData("Thread updated", json_encode($thread->toArray()));
			} else {
				$this->logData("Unable to update thread", json_encode($thread->toArray()));
			}
			$this->logData("New message saved", json_encode($gciThreadMessage->toArray()));
		  } else {
		 	$this->logData("Unable to save new message", json_encode($gciThreadMessage->toArray()));
		   }
	 	   return $gciThreadMessage;	
	  	 }
		return $gciThreadMessages[0];
	}
	public function createThreadIfNeeded($user,$message,$sender,$time,$subject,$decoded) {
		$gciThread = new \app\models\GciThreads;
		$gciThreads = $gciThread->find()->where([
			'gci_thread_uid' => $message->getThreadId()
		])->all();
		if ( $gciThreads ) {
			return $gciThreads[0];
		  }
		$gciThread->gci_user_id=$user->id;
		$gciThread->gci_thread_title=$subject;
	   	$gciThread->gci_thread_email=$sender;
		$gciThread->gci_thread_date_created=$time->format("Y-m-d H:i:s");
		$gciThread->gci_thread_date_updated=$time->format("Y-m-d H:i:s");
		$gciThread->gci_thread_uid=$message->getThreadId();
		if (  !$gciThread->save() ) {
			$this->logData("ERROR THREAD SAVE", json_encode($gciThread->toArray()));
		}  else {
			$this->logData("NEW THREAD", json_encode($gciThread->toArray()));
		 }
		return $gciThread;
	  }
			
 


	public function getFullAutoreply($autoresponder, $user) {
		return sprintf("%s\r\n\r\n",$autoresponder->gci_autoresponder_message,$user->gci_user_signature);
	 }

	  
	public function getSender($user,$message,$lines) {
		//$lines = $this->getMimeLines($decoded);
		foreach($lines as $lineName => $lineValue) {
			if ( ((strtolower($lineName) == "from" ) && $user->gci_user_email_address !== $lineValue)
			|| (strtolower($lineName) == "to") && $user->gci_user_email_address !== $lineValue) {
			     // message should be in <user@email.com>
			    $this->logData("Reading Sender: ", $lineValue);	
			    $emailAddress = preg_match("/\<(.*)\>/", $lineValue, $matches );
			    if  ( $matches ) {
			    	return $matches[1];
			    }
			 }
		}
	 	return false;
	}
	public function getSubject($message,$lines) {
		 foreach($lines as $lineName => $lineValue) {
			 if (strtolower($lineName) == "subject") {
			     return $lineValue;
			  }
		   }
		 return false;
	 }

   	public function getMimeLines($decoded) {	
	  	 $lines = explode("\r\n",$decoded);
		$mimeKeyPair = [];
		 foreach($lines as $line) {
			$split = explode(":", $line);
			if ( !empty($split) && sizeof($split)>1) {
				if ( sizeof($split)>2) {
					$value=implode(":",  array_slice($split,1,sizeof($split)-1));
				}  else {
					$value=$split[1];
				}

				$mimeKeyPair[$split[0]]=str_replace("\n", "", str_replace("\r\n", "", $value));
			  }
		  }
		//$this->logData("MIME Headers", json_encode($mimeKeyPair));
		 return $mimeKeyPair;
	 }
	public function getSignature($message, $sender,$time, $decoded) {
		$signatureObject=new \stdClass;
		$signatureObject->time = $time;
		$signatureObject->sender = $sender;
		return $signatureObject;
	 }
	public function getTime($message, $lines) {
		//$lines = $this->getMimeLines($decoded);
		foreach($lines as $lineName=> $lineValue ) {
			if (strtolower($lineName)=="date") {
			  $dateTime = new \DateTime;
			  //$this->logData("Reading Date: ", $lineValue);
			  $dateTime->setTimestamp(strtotime( $lineValue));
			  return  $dateTime;
			 }
		}
		return false;
	}
	public function logData($details,$message) {
		printf("LOG: %s, %s\r\n", $details, $message);
	 }
	// does a message need  an  auto reply
	public function needsAutoreply( $message,$sender, $time, $cache,$config, $decoded ) {
		$signature = $this->getSignature( $message,$sender,$time,$decoded );
		foreach ( $cache[$sender] as  $sentMessage ) {
			if ( $signature->time == $sentMessage->time &&
				$signature->sender == $sentMessage->sender ) {
				return false;
			}
		}
		// check for time

		$timeElapse = time() - ($config->EMAILER->REPLY_TIME_ELAPSE);
		if ( $time->getTimestamp() > $timeElapse ) {
			return true;
		}
		return false;
	}
	public function sendAutoreply($user,$message,$sender,$time, $gmailClient, $googleClient,$decoded, $cache) {
		//$sender = $this->getSender($message);
		$autoresponder = new \app\models\GciAutoresponders;
		$autoresponder = $autoresponder->find()->where([
			'gci_user_id' => $user->id
			])->one();
		$this->logData("Trying to send message to: ", $sender);
		if ( $autoresponder && $autoresponder->gci_autoresponder_active ) {
			$fullMessage = $this->getFullAutoreply($autoresponder,$user);
			$this->logData("Message Match OK and AutoResponder ON, sending message now ", $fullMessage);
			//$fullSubject = $this->getull
			
			$fullSubject = $autoresponder->gci_autoresponder_subject;
			$mailMime = new \Mail_mime;
			$mailMime->addTo($sender);
			$mailMime->setTXTBody($fullMessage);
			$mailMime->setHTMLBody($fullMessage);
			$mailMime->setSubject($fullSubject);
			$raw = base64url_encode($mailMime->getMessage());
			$gmailMessage = new \Google_Service_Gmail_Message();
			$gmailMessage->setRaw($raw);
			//$this->logData("Sending Autoresponse", $mailMime->getMessage());
			//$newMessage = $gmailClient->messages->send("me", $gmailMessage);
			//$this->logData("Sent Message ID: ", $newMessage->getId());
			$cache[$sender][]=$this->getSignature($message, $sender,$time,$decoded);
		  } else {
			$this->logData("AutoResponder OFF", "");
		 }
		return $cache;
	 }
	   
				

}


