<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function actionHome() {
	return $this->render("home", []);
	}
    public function actionMain() {
	 return $this->render("main", []);
     }
    public function actionLogin(){
	 return $this->render("login", []);
    }
    public function actionLogout() {
	 return $this->render("logout", []);
    }
    public function actionMore() {
	 return $this->render("more", []); 
    }
    public function actionListmail() {
	 return $this->render("listmail", []);
    }
    public function actionListthreads() {
	 return $this->render("listthreads", []);
    }
    public function actionListmessages() {
	 return $this->render("listmessages", []);
    }
    public function actionListmessagesbox() {
	 $this->layout = "json";
	 return $this->render("listmessages", []);
    }
    public function actionError() {
	  return $this->render("error", []);
   }
    public function actionLogs() {
	  return $this->render("logs", []);
    }

    public function actionSendmail() {
	  return $this->render("sendmail", []);
 	}
    public function actionSetupmail() {
	  return $this->render("setupmail", []);
    }
    public function actionSetupcannedreply() {
	 return $this->render("setupcannedreply",[]);
    }
    public function actionSetupsignature() {
	   return $this->render("setupsignature", []); 
    }
    public function actionSetupnotes() {
	   return $this->render("setupnotes", []);
    }
   
    public function actionAuthtoken() {
	 $authUrl = Yii::$app->GmailComponents->getAuthUrl();
	 header("Location: $authUrl");
	 exit;
    }
    public function actionGranttoken(){
	 $session  = new \yii\web\Session;
	 $request = new \yii\web\Request;
	 $user = new \app\models\GciUsers;
	 $user = $user->findOne($session->get("user")['id']);
	$accessToken = Yii::$app->GmailComponents->getAccessToken( $request->get("code") ) ;
	 if ( $accessToken ) {
		 $user->gci_user_gmail_auth = $accessToken;
		 if ( $user->update() ) {
		 	return $this->redirect("?r=site/listmail");
		  } else {
			return $this->redirect("?r=site/error");
		 }
	  } else {
	  	return $this->redirect("?r=site/error");
	  }
	}
	 

 
}
