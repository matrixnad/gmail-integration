<?php

namespace app\controllers;

use yii\web\Controller;
use yii\helpers\ArrayHelper;
use Yii;
final class ApiController extends Controller {
	public $layout = "json";
	public $enableCsrfValidation = false;
	public function beforeAction($action) {
		$session = new \yii\web\Session;
		if ( !in_array($action->id,['login', 'logout']) && !$session->get("master") )  {
			return false;
		}
	  	return true;
	 }
	    			


	public function responseMessages() {
		return [ 
			'SUCCESS' => [
				'LISTMAIL_FOUND' => 'Listing of auto responders',
				'LISTUSER_FOUND' => 'Listing of users',
				'LISTCANNEDREPLIES_FOUND' => 'Listing of canned replies',
				'LISTTHREAD_FOUND' => 'Listing of threads',
				'LISTTHREADMESSAGES_FOUND' => 'Listing of messages',
				'LOG_FOUND' => 'Logs were found',
				'USER_FOUND' => 'User was found',
				'THREAD_FOUND' => 'Thread was found',
				'MESSAGE_FOUND' => 'Message was found',
				'SIGNATURE_UPDATED' => 'Signature was updated',
				'AUTORESPONDER_OK' => 'Auto responder was updated',
				'THREAD_NOTES_OK' => 'The thread notes were updated',
				'CANNEDREPLY_OK' => 'Canned reply message saved',
				'SWITCHED_ACCOUNT' => 'Switched accounts',
				'EMAIL_SENT' => 'Email was sent',
				'LOGGEDOUT' => 'Logged out of account'
			],
			'ERRORS' => [
				'LISTMAIL_NOTFOUND' => 'Could not get list mail data',
				'USER_NOTFOUND' => 'Could not get user data',
				'THREAD_NOTFOUND' => 'Could not find thread',
				'MESSAGE_NOTFOUND' => 'Could not find message',
				'AUTORESPONDER_SUBJECT_INVALID' => 'The auto responder subject is invalid',
				'AUTORESPONDER_MESSAGE_INVALID' => 'The auto responder message is invalid',
				'CANNEDREPLY_SUBJECT_INVALID' => 'The canned reply message invalid',
				'CANNEDREPLY_MESSAGE_INVALID' => 'The canned reply subject is invalid',
				'THREAD_NOTE_INVALID' => 'The notes for this thread were invalid',
				'SIGNATURE_INVALID' => 'Signature is invalid',
				'DATABASE_ERROR' => 'Something went wrong in the database',
				'EMAIL_ERROR' => 'Email could not be sent'
				]
		];
	}
		

	public function getWithOffset($data,$dataObject,$lookupArray,$returnArray) {
		$session = new \yii\web\Session;
		$page= $data['page'];
		$limit = 1000;
		$pagelimit=$page*$limit;
		$results = $dataObject->find()->where( $lookupArray )->limit($limit)->offset($pagelimit);
		$count =  $dataObject->find()->where($lookupArray)->count();
		$all = $results->all();
	   	if ($count > 0 ) {	
			$pages= floor(($count/1000));
		 } else {
			$pages = 0;
		}
		$arrayResults = ['results' =>ArrayHelper::toArray($all,$returnArray), 'count' => $count,
			'pages' => $pages
			];
		return $arrayResults;
			
	}
		
	public function responseObject($type=FALSE,$message='',$data=[]) {
		$object = new \stdClass;
		$object->error=$type;
		$object->message=$message;
		$object->data=$data;
		return $object;
	 }
	public function successResponse($message,$data) {
		return $this->responseObject(FALSE,$message,$data);
	}
	public function errorResponse($message,$data) {
		return $this->responseObject(TRUE,$message,$data);
	}
	public function getData() {
		return json_decode(file_get_contents("php://input"),TRUE);
	}

	public function response($object) {
		$apiCodes = [
			'OK' => 'OK',
			'ERROR' => 'ERROR'
		];
		return $this->render("response", [
				'data' => json_encode([
					'data' => $object->data,
					'message' => $object->message,
					'status' => $object->error? $apiCodes['ERROR'] : $apiCodes['OK']
					])
			]);
	}

	public function actionGetthread() {
		 $session = new \yii\web\Session; 
		 $thread = new \app\models\GciThreads;
		$data = $this->getData();
		 $thread = $thread->findOne( $data['id'] );
		 $apiResponses = $this->responseMessages();
		 if ( $thread ) {
			$successObject = $this->successResponse($apiResponses['SUCCESS']['THREAD_FOUND'],$thread->toArray());
			return $this->response($successObject);
		  } else {
			$errorObject = $this->errorResponse($apiResponses['ERRORS']['THREAD_NOTFOUND'],[]);
			return $this->response($errorObject);
		  }
	 }
	public function actionGetmessage() {
		$apiResponses = $this->responseMessages();
		$session = new \yii\web\Session;
		$message = new \app\models\GciThreadsMessages;
		$user = new \app\models\GciUsers;
		$user = $user->findOne($session->get("user")['id']);
		$data = $this->getData();
		$message = $message->find()->where([
			'gci_message_uid' =>$data['messageUid']
			])->all();
		if ( $message ) {
			$message = ArrayHelper::toArray($message,[
				'app\models\GciThreadsMessages' => [
					'gci_message_uid'
				]
			]);
			$message = Yii::$app->GmailComponents->getMessages($user,$message);
			$successResponse = $this->successResponse($apiResponses['SUCCESS']['MESSAGE_FOUND'], $message[0]);
			return $this->response($successResponse);
		} else {
			$errorResponse = $this->errorResponse($apiResponse['ERROR']['MESSAGE_NOTFOUND'] , []);
			return $this->response($errorResponse);
		}
	}


			
	// list mail also returns user
	public function actionListmail() {
		$session = new \yii\web\Session;
		$responseMessages = $this->responseMessages();
		$autoresponder = new \app\models\GciAutoresponders;
		$autoresponder = $autoresponder->find()->where([
			'gci_user_id' => (int) $session->get("user")['id']
			])->all();
		$user = new \app\models\GciUsers;
		if ( $autoresponder ) {
		  	$userObj = $user->findOne($session->get("user")['id'])->toArray();
			$responseData =['user' => $userObj, 'autoresponder'=> $autoresponder[0]->toArray()];
			$successObject = $this->successResponse($responseMessages['SUCCESS']['LISTMAIL_FOUND'], $responseData);
			return $this->response($successObject);
		} else {
			$errorObject = $this->errorResponse($responseMessages['ERROR']['LISTMAIL_NOTFOUND'], []);
			return $this->response($errorObject);
		}
	}
	 //TODO move to getWithOffset
	public function actionListnotethreads() {
		$session = new \yii\web\Session;
		$responseMessages = $this->responseMessages();
		$threads = new \app\models\GciThreads;
		$data = $this->getData();
		$page=$data['page'];
		$limit=1000;
		$pagelimit=$page*$limit;
	 	$lookupArray = [
			'gci_user_id' => $session->get("user")['id'] 
			];

		$results = $threads->find()->where($lookupArray)->limit($limit)->offset($pagelimit)
				->groupBy([
				'gci_thread_uid'
			])
			->orderBy([
				'gci_thread_date_updated' => SORT_DESC
			])
			->all();
		$count = $threads->find()->where($lookupArray)->count();
		if ( $count>0) {
			$pages = floor($count/$limit);
		}  else {
			$pages=0;
		}
		$array = ArrayHelper::toArray($results, [
			'app\models\GciThreads' => [
				'gci_thread_uid',
				'gci_thread_title',
				'gci_thread_date_created',
				'id'
				]]);
		$result = ['results' =>$array,'pages' =>$pages,'count' =>$count];
		$successResponse = $this->successResponse($responseMessages['SUCCESS']['LISTTHREAD_FOUND'], $result);
		return $this->response($successResponse);
	 }
	public function actionListnotethreadmessages() {
		$session = new \yii\web\Session;
		$responseMessages = $this->responseMessages();
		$messages = new \app\models\GciThreadsMessages;
		$thread = new \app\models\GciThreads;
		$data = $this->getData();
		$user = new \app\models\GciUsers;
		$user = $user->findOne($session->get("user")['id']);
		$thread = $thread->find()->where([
			'gci_thread_uid' => $data['threadUid']
			])->one();
		$messages = $this->getWithOffset($data, $messages, [
			'gci_thread_uid' =>$data['threadUid']
		],  [ 
			'app\models\GciThreadsMessages' => [
					'gci_message_uid',
					'gci_thread_uid'
			] ] );
		 $messages['results'] = Yii::$app->GmailComponents->getMessages($user,$messages['results']);
		 $messages['thread'] = $thread->toArray();
		 $successResponse = $this->successResponse($responseMessages['SUCCESS']['LISTTHREADMESSAGES_FOUND'],$messages);
		return $this->response($successResponse);
	 }

	public function actionListusers() {
	     	$responseMessages = $this->responseMessages();
		$users = new \app\models\GciUsers;
			
		$inArray =  ArrayHelper::toArray( $users->find()->where([])->all(), [
			'app\models\GciUsers' => [
				'id',
				'gci_user_email_address',
				'gci_user_password',
				'gci_user_fullname',
				'gci_user_signature'
				]
			  ] );
		$successResponse = $this->successResponse($responseMessages['SUCCESS']['LISTUSER_FOUND'], $inArray);
		return $this->response($successResponse);
	 }
	public function actionListcannedreplies() {
		$responseMessages = $this->responseMessages();
		$cannedReplies = new \app\models\GciCannedreplies;
		$cannedRepliesArray = ArrayHelper::toArray($cannedReplies->find()->where([])->all(), [
			'app\models\GciCannedreplies' => [
				'gci_cannedreply_name',
				'gci_cannedreply_subject',
				'gci_cannedreply_message'
				] ] ) ;
		$successResponse = $this->successResponse($responseMessages['SUCCESS']['LISTCANNEDREPLIES_FOUND'], $cannedRepliesArray);
		return $this->response($successResponse);
	 }
	public function actionLogs() {
		$responseMessages = $this->responseMessages();
		$data = $this->getData();
		$session = new \yii\web\Session;
		$logMessages = new \app\models\GciLogs;
		$logMessages=  $logMessages->find()->where([
			'gci_log_date_created' => $data['gci_log_date_created'],
			'gci_user_id' => $session->get("user")['id']
			])->all();
	 }
	public function actionLogin() {
		$data = $this->getData();
		$responseMessages = $this->responseMessages();
		$user = new \app\models\GciMasterAccounts;
		$user = $user->find()->where($data)->all();
		if ( $user ) {
			$userInArray=$user[0]->toArray();
			$session = new \yii\web\Session;
			//$session->set("user", $userInArray);
			$session->set("master", $userInArray);
			$successObject = $this->successResponse($responseMessages['SUCCESS']['USER_FOUND'],$userInArray);
			return $this->response($successObject);
		} else {
			$errorObject = $this->errorResponse($responseMessages['ERRORS']['USER_NOTFOUND'], []);
			return $this->response($errorObject);
		}
	 }
	public function actionLogout() {
	    	$session = new \yii\web\Session;
		$responseMessages = $this->responseMessages();
		if ($session->get("master")) {
			$session->remove("master");
		}
		if ($session->get("user")) {
			$session->remove("user");
		}
		$response = $this->successResponse($responseMessages['SUCCESS']['LOGGEDOUT'], []);
		return $this->response($response);
	 }
	public function actionSetupmail() {
		$session = new \yii\web\Session;
		$data = $this->getData();
		$responseMessages = $this->responseMessages();
		$autoresponder = new \app\models\GciAutoresponders;
		$autoresponder = $autoresponder->find()->where([
			'gci_user_id' => $session->get("user")['id']
		])->all();
		$successResponse = $this->successResponse("", []);
		$errorResponse = $this->errorResponse("", []);
		if ( ! Yii::$app->ValidateComponents->validateAutoresponderSubject ($data) )   {
			$errorResponse->message=$responseMessages['ERRORS']['AUTORESPONDER_SUBJECT_INVALID'];
			return $this->response($errorResponse);
		} elseif ( !Yii::$app->ValidateComponents->validateAutoresponderMessage( $data ) ) { 
			$errorResponse->message=$responseMessages['ERRORS']['AUTORESPONDER_MESSAGE_INVALID'];
			return $this->response($errorResponse);
		} else {
			$autoresponder = $autoresponder[0];
			$autoresponder->gci_autoresponder_subject=$data['gci_autoresponder_subject'];
			$autoresponder->gci_autoresponder_message=$data['gci_autoresponder_message'];
			$autoresponder->gci_autoresponder_active=$data['gci_autoresponder_active'];
			//$autoresponder->gci_autoresponder_notes=$data['gci_autoresponder_notes'];
			if ($autoresponder->update() !== FALSE) {
				$successResponse->message = $responseMessages['SUCCESS']['AUTORESPONDER_OK'];
				return $this->response($successResponse);
			} else {
				$errorResponse->message = $responseMessages['ERRORS']['DATABASE_ERROR'];
				return $this->response($errorResponse);
			}
		}
	}
	public function actionSetupsignature(){
		$data = $this->getData();
		$responseMessages = $this->responseMessages();
		$session = new \yii\web\Session;
		$user = $session->get("user");
		$userDb = new \app\models\GciUsers;
		$userDb = $userDb->findOne( $user['id'] );
		if ( $userDb ) {
			if ( !Yii::$app->ValidateComponents->validateSignature( $data ) ) {
				$errorResponse = $this->errorResponse($responseMessages['ERRORS']['SIGNATURE_INVALID'],[]);
				return $this->response($errorResponse);
			} else {
				$userDb->gci_user_signature=$data['gci_user_signature'];
				if ( $userDb->update()  !== FALSE) {
					$successResponse=$this->successResponse($responseMessages['SUCCESS']['SIGNATURE_UPDATED'], []);
					return $this->response($successResponse);
				} else  {
					$errorResponse = $this->errorResponse($responseMessages['ERRORS']['DATABASE_ERROR'], []);
					return $this->response($errorResponse);
				}
			}  
		}
       }
      public function actionSetupcannedreply() {
		$data = $this->getData();
		$responseMessages = $this->responseMessages();
		$cannedReply = new \app\models\GciCannedreplies;
		if ( !Yii::$app->ValidateComponents->validateCannedreplySubject( $data ) ) { 
		     $errorResponse = $this->errorResponse($responseMessages['ERRORS']['CANNEDREPLY_SUBJECT_INVALID']);
		     return $this->response($errorResponse);
		} elseif ( !Yii::$app->ValidateComponents->validateCannedreplyMessage( $data ) ) {
		     $errorResponse = $this->errorResponse($responseMessages['ERRORS']['CANNEDREPLY_MESSAGE_INVALID']);
		     return $this->response($errorResponse);
		 } else {
		     $cannedReply->gci_cannedreply_subject=$data['gci_cannedreply_subject'];
		     $cannedReply->gci_cannedreply_message=$data['gci_cannedreply_message'];
	  	     if ($cannedReply->save()) {
			     $successResponse = $this->successResponse($responseMessages['SUCCESS']['CANNEDREPLY_OK'], $cannedReply->toArray());
			     return $this->response($successResponse);
		     } else {
			 $errorResponse = $this->errorResponse($responseMessages['ERRORS']['DATABASE_ERROR'], []);
			  return $this->response($errorResponse);
		     }
		 }
	}
      public function actionSetupnote() {
		$data = $this->getData();
		$responseMessages = $this->responseMessages();
		$thread = new \app\models\GciThreads;
		$thread = $thread->findOne($data['id']);
		if ( $thread ) {
			if ( !Yii::$app->ValidateComponents->validateThreadNotes( $data )) {
			   	 $errorResponse = $this->errorResponse( $responseMessages['ERRORS']['THREAD_NOTES_INVALID'], []);
				return $this->response($errorResponse);
			 }  else {
				$thread->gci_thread_notes = $data['gci_thread_notes'];
				if ($thread->update() !== FALSE) {
					$successResponse = $this->successResponse( $responseMessages['SUCCESS']['THREAD_NOTES_OK'], $thread->toArray());
					return $this->response( $successResponse );
				} else {
					$errorResponse = $this->errorResponse( $responseMessages['ERRORS']['DATABASE_ERROR'], [] );
					return $this->response( $errorResponse );
				}
			}
		}
	}
			 
      public function actionSendmail() { 
		$responseMessages = $this->responseMessages();
		$data = $this->getData();
		$session = new \yii\web\Session;
		$user = new \app\models\GciUsers;
		$user = $user->findOne($session->get("user")['id']);
		
		$mailing = Yii::$app->GmailComponents->sendMail(
			$user,
			$data['to'],
			$data['cc'],
			$data['bcc'],
			$data['subject'],
			$data['message']
			);
		if ( $mailing ) {
			$successResponse = $this->successResponse( $responseMessages['SUCCESS']['EMAIL_SENT'], [] );
			return $this->response($successResponse);
		} else {
			$errorResponse = $this->errorResponse( $responseMessages['ERROR']['EMAIL_ERROR'], [] );
			return $this->response($errorResponse);
		}
      }

      public function actionSwitchuser() {
		$responseMessages = $this->responseMessages();
		$data = $this->getData();
		file_put_contents(__DIR__."/../log.txt", json_encode($data));
		$session = new \yii\web\Session;
		$session->set("user", $data);
		$successObject = $this->successResponse($responseMessages['SUCCESS']['SWITCHED_ACCOUNT'],$data);
		return $this->response($successObject);
	 }
  
     public function actionUserandautoresponder() {
	}
     public function actionUser() {
	$responseMessages = $this->responseMessages();
	$session = new \yii\web\Session;
	if ( $session->get("user")) {
		$successObject = $this->successResponse($responseMessages['SUCCESS']['USER_FOUND'],$session->get("user"));
		return $this->response($successObject);
	  } else {  
		$errorObject = $this->errorResponse($responseMessages['ERRORS']['USER_NOTFOUND'],[]);
		return $this->response($errorObject);
		}
	 }

		 
}
?>
